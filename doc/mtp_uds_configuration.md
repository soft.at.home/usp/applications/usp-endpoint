# MTP UDS configuration

## Introduction

The USP Endpoint has an implementation for the MTP of type UDS (UnixDomainSockets). This MTP will mainly be used to connect containers on the HGW with the host system and to allow the container and host to communicate using USP messages.

## Architecture

The UDS has the following specifications:

- It allows message exchange between 2 processes using UNIX domain sockets
- The messages are TLV (Type-Length-Value) messages and there are currently 3 Types used for the UDS MTP: type Handshake, type Protobuf and type Error
- TLVs of type protobuf contain a protobuf encoded USP message as specified by [TR-369 USP](https://usp.technology/specification/07-index-messages.html#sec:messages)
- TLVs of type Handshake contain a string with a USP EndpointID in the format specified by [USP: Endpoint Identifier](https://usp.technology/specification/02-index-architecture.html#sec:endpoint-identifier)
- Multiple UDS connections can be set up if communication with several processes is needed

The UDS MTP currently has a reference parameter to the socket that is used:

- `Device.­Local­Agent.­MTP.­{i}.­UDS.UnixDomainSocketRef`: reference to an instance in the `Device.UnixDomainSockets.UnixDomainSocket.{i}.` table.

This UnixDomainSocket instance has 2 parameters:

- Path: filesystem path to the socket
- Mode: can be Listen or Connect Mode
    - In case of Listen Mode, the USP Endpoint will set up a listen socket for USP services to connect to.
    - In case of Connect Mode, the USP Endpoint will connect to a listen socket of another USP process, which is called a USP Broker in the High Level API architecture.

If 2 processes need to be connected over the UDS MTP, you will need an MTP in Listen Mode on one side and an MTP in Connect Mode on the other side. Our goal is to connect the data models on the host system with the data models in the container. Therefore we will typically have a USP agent on the host with an MTP in Listen Mode (also known as the USP Broker) and a USP Endpoint in the container with an MTP in Connect mode. So the USP agent on the host sets up listen sockets for the USP Endpoint in the container to connect to.

> NOTE  
> According to the specifications, controllers can only send messages to agents and agents can only send messages to controllers. To facilitate this, the USP Broker will set up 2 listen sockets: one for agents to connect on and one for controllers to connect one. This provides a clear separation of communication between agents and controllers.

Unfortunately this host-container connection is not enough to make the system work. Up until now the USP agent on the host has no idea which data models are present in the container. To solve this the USP Endpoint will send a USP Register messages to the USP agent to announce which data models it provides.

The USP Endpoint has a similar problem as the USP agent: it doesn't know which data models are available inside the container, but it should be able to discover this dynamically. There are again several solutions for this problem. For now we have decided that each Ambiorix plug-in in the container that wants to register its data model needs to send a USP Register message to the USP Endpoint. Don't panic yet; this does not mean that every ambiorix component needs to be updated. Sending the Register message will be done automatically whenever the Ambiorix plug-in connects over the USP back-end to the USP Endpoint, meaning that not a single line of code needs to be updated. We just need to add a configuration setting in the odl file to make the plug-in connect to the USP Endpoint.

You may be wondering: where exactly should the Ambiorix plug-ins connect to if the USP Endpoint has a UDS MTP in Connect Mode? The answer is pretty straightforward: the USP Endpoint will also have a UDS MTP in Listen Mode for all the container plug-ins to connect to.

In summary, the USP agent on the host will set up two listen socket for the USP Endpoint to connect to. The USP Endpoint sets up its own listen sockets for ambiorix plug-ins to connect to. Upon connection to the USP Endpoint's Controller socket, the ambiorix plugin will send a Register message to the USP Endpoint, which will forward this to the USP agent on the host. The USP agent will track which data models are registered on each of its connections, so it will always know where to forward incoming requests, regardless of the number of UDS connections it has.

The image below gives a visual overview of how everything will be connected.

![mtp-uds](mtp-uds.png)

The image shows a basic example of a HGW with a USP agent and a few other components connected to the system bus on the left. The CLI application is connected to both the system bus and the listen sockets of the USP agent. The CLI will use the controller connection to the USP agent whenever it wants to access data models in the container and can use the bus connection to access local data models on the system bus of the HGW. Note that the CLI could also access local data models via the UDS controller connection to the USP agent. The USP agent will just forward incoming requests on one of its connections, so it can also forward them on the bus connection.

On the right side of the image, we have a container installed on the HGW with a very similar setup as outside of the container. There is a USP Endpoint with a few other components that are connected to the system bus in the container. All components in the container are connected to the listen sockets of the USP Endpoint in order to register their data models (as agents) and access the host system (as controllers). The CLI inside the container doesn't have a data model to register, but it will still need to use the UDS controller connection if it wants to access data models on the host system.

Note that all components in green are Ambiorix components which don't require any code changes to work in this architecture. Some of the just need to connect to the right listen sockets, but this is a configuration detail that is handled automatically by amxrt and the USP back-end.

## How do Ambiorix plug-ins access data models in the container and vice versa

Assume you have an Ambiorix plug-in on the host which is connected to the local system bus and to the USP agent. The connection made to the bus and agent will both be done with an `amxb_connect`. As a result, the Ambiorix plug-in can use [amxb_be_who_has](http://sah2artifactory01.be.softathome.com:10000/documentation/ambiorix/libraries/libamxb/master/db/d57/a00112.html#ga088c22b348726a9d0d05acdb8de37d3b) to figure out which bus context to use for making requests. The first context that can reach the requested object will be returned, so for local objects, the bus context will be returned and for container objects, the connection context to the USP Agent will be returned (assuming the object was registered correctly).

Inside the container the exact same reasoning can be used. Ambiorix plug-ins will connect to the USP Endpoint in the container and to the local bus in the container using `amxb_connect`. If the requested object can be reached through the USP Endpoint, its 'bus' context will be returned.

## USP EndpointIDs

If you're familiar with USP, you may know that USP EndpointIDs are quite important when communicating over USP. Each USP message must contain a `from_id` and `to_id`, so before any USP messages can be sent, you must know who to send it to and you should also know your own ID. For USP Endpoints with a LocalAgent or LocalController data model, figuring out their own EndpointID is quite straightforward, because it can be found under the root object. For Ambiorix plug-ins connecting over the USP back-end, the EndpointID can be configured under the `usp` section in the config odl. For example:

```odl
%config {
    usp = {
        EndpointID = "user::abcd1234"
    };
}
```

This is completely optional though and if it is missing, the USP back-end will automatically generate an EndpointID for the plug-in.

We've made it a convention that the connecting process will always start the communication by sending its EndpointID over the UDS with a Handshake TLV and it will receive an EndpointID as a Handshake response. This is again handled automatically by the USP back-end. After the EndpointID exchange is done, the plug-in can use any of the `amxb` operations as usual.

## Data model configuration

Below is an example odl that can be used to configure the IMTP of the USP Endpoint in a container

```odl
%populate {
    object LocalAgent {
        parameter Enable = true;
        object MTP {
            instance add (0, "mtp-uds-host") {
                extend with mib UDS;

                parameter Enable = true;
                parameter Protocol = "UDS";

                object UDS {
                    parameter UnixDomainSocketRef = "";
                }
            }

            instance add (0, "mtp-uds-container") {
                extend with mib UDS;

                parameter Enable = true;
                parameter Protocol = "UDS";

                object UDS {
                    parameter UnixDomainSocketRef = "";
                }
            }
        }
    }
}
```

## Run time prerequisites

- [amxb_usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp)
- [amxrt](https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt)
- [mod_sahtrace](https://gitlab.com/prpl-foundation/components/core/modules/mod_sahtrace) (optional)

To setup the communication between the USP Endpoint and the USP agent on the host or the Ambiorix plug-ins in the container, and to exchange messages between these processes, the USP back-end is used. Because it is a back-end, it is loaded dynamically so it is technically not a build time requirement. However if it is not installed, the connection setup will fail.

Like most ambiorix plug-ins, the USP Endpoint will run with the Ambiorix runtime `amxrt`, so this also needs to be installed.

If you want to have trace logs available for these components, you can install [mod_sahtrace](https://gitlab.com/prpl-foundation/components/core/modules/mod_sahtrace). This is an optional module that will be imported if it is installed. If you are not interested in trace logs, you can skip this installation.

The components mentioned above can be installed from source or using their debian packages. To install the debian packages, run

```bash
sudo apt update
sudo apt install mod-amxb-usp amxrt mod-sahtrace
```

## IMPORTANT

If you made it this far in the document, we would like to thank you for your time, but would like to point out the following important points:

- The USP Endpoint behaves as both a USP agent and controller. To run it, you can use the symbolic link `uspe`.

## Future extensions

- Discovery of native pcb/ubus components in containers.
- A discovery data model inside the container to completely remove the need for a bus inside the container. In other words: the USP Endpoint will act as the bus.
- Consider making the whole system asynchronous.
- Allow using the LocalController.usp_... operations over the UDS MTP
