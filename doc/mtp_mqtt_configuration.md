# MTP MQTT configuration

## Introduction

## Run time prerequisites

- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt)
- [amxb_usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp)
- [amxrt](https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt)
- [mod_sahtrace](https://gitlab.com/prpl-foundation/components/core/modules/mod_sahtrace) (optional)

If you want to use MQTT as the MTP to communicate, the `tr181-mqtt` client needs to be installed.

To setup the communication between the USP Endpoint (agent/controller) and the MQTT client and to exchange messages between these processes, the USP back-end is used. Because it is a back-end, it is loaded dynamically so it is technically not a build time requirement. However if it is not installed, it will not be possible to publish USP messages over MQTT.

Like most ambiorix plug-ins, the USP Endpoint will run with the ambiorix runtime `amxrt`, so this also needs to be installed.

If you want to have trace logs available for these components, you can install [mod_sahtrace](https://gitlab.com/prpl-foundation/components/core/modules/mod_sahtrace). This is an optional module that will be imported if it is installed. If you are not interested in trace logs, you can skip this installation.

The components mentioned above can be installed from source or using their debian packages. To install the debian packages, run

```bash
sudo apt update
sudo apt install tr181-mqtt mod-amxb-usp amxrt mod-sahtrace
```

## Configuration

Below is an example odl that can be used to configure the MQTT MTP of the USP controller

```odl
%populate {
    object LocalController {
        parameter Enable = true;
        parameter ActiveMTP = "LocalController.MTP.mtp-mqtt-controller";
        
        object MTP {
            instance add (0, "mtp-mqtt-controller") {
                extend with mib MQTT;

                parameter Enable = true;
                parameter Protocol = "MQTT";

                object MQTT {
                    parameter Reference = "MQTT.Client.cpe-mqtt-uspc.";
                }
            }
        }
    }
}
```

Note that the data model has a `LocalController.ActiveMTP` parameter which needs to be set to select the MTP you want to use for sending USP operations. At the moment only MQTT is supported, so you will have to use this MTP instance. You can use `pcb-cli` or `ubus-cli` to set the `ActiveMTP` parameter to this default instance or you can pre-configure it in the defaults file.

During the installation of the agent and controller, you will also see that 2 odl files are added to the defaults odl directory of the MQTT client.

```
/usr/bin/install -D -p -m 0644 odl/mqtt-client-uspa.odl /etc/amx/tr181-mqtt/defaults/odl/01-mqtt-client-uspa.odl
/usr/bin/install -D -p -m 0644 odl/mqtt-client-uspc.odl /etc/amx/tr181-mqtt/defaults/odl/01-mqtt-client-uspc.odl
```

The contents of `mqtt-client-uspc.odl` are as follows

```
%populate {
    object MQTT.Client {
        instance add (0, "cpe-mqtt-uspc") {
            parameter BrokerAddress="broker.hivemq.com";
            parameter Enable = false;
        }
    }
}
```

As you can see, this adds an extra `MQTT.Client` instance on startup of `tr181-mqtt`. The USP controller will use this client instance to publish its MQTT requests and receive its MQTT responses. A different `MQTT.Client` instance is added for the USP agent to communicate over MQTT. These are installed in separate files, because you could install the controller on one machine and the agent on another machine.
