# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.0.9 - 2024-09-30(05:51:42 +0000)

### Other

- CI: Disable squashing of open source commits
- [AMX] Disable commit squashing

## Release v2.0.8 - 2024-03-27(16:35:14 +0000)

### Other

- [USP] Extend documentation

## Release v2.0.7 - 2024-01-18(11:36:46 +0000)

### Fixes

- [USP] usp-endpoint needs to load usp backend

## Release v2.0.6 - 2023-11-28(09:28:56 +0000)

### Fixes

- [USP] Describe returns list with result

## Release v2.0.5 - 2023-09-18(09:54:37 +0000)

### Other

- [usp-endpoint] Fix baf output and remove () from condition check

## Release v2.0.4 - 2023-09-15(10:54:46 +0000)

### Other

- [usp-endpoint] Install uspa_definition.odl when CONFIG_SAH_SERVICES_USPE is enabled

## Release v2.0.3 - 2023-08-29(08:58:39 +0000)

### Other

- Recommend using allow_partial = true

## Release v2.0.2 - 2023-08-29(08:03:25 +0000)

### Fixes

- [USP] Notify response published on wrong topic

## Release v2.0.1 - 2023-07-14(12:56:15 +0000)

### Other

- [USP] Add config variables for init script of usp-endpoint

## Release v2.0.0 - 2023-07-11(11:21:35 +0000)

### Breaking

- [IMTP] Implement IMTP communication as specified in TR-369

## Release v1.2.3 - 2023-06-06(06:50:41 +0000)

### Other

- Extend README with runtime dependencies

## Release v1.2.2 - 2023-05-26(07:27:37 +0000)

### Fixes

- [USP][MQTT] Missing unique keys for MQTT data model

## Release v1.2.1 - 2023-05-11(12:06:12 +0000)

### Fixes

- [USP] Controllers with are added with an invalid EndpointID

## Release v1.2.0 - 2023-05-02(11:16:51 +0000)

### New

- [USP] usp-endpoint must be able to broker amx subscriptions

## Release v1.1.2 - 2023-04-13(13:52:39 +0000)

### Changes

- [USP] Port subscription changes to usp-endpoint

## Release v1.1.1 - 2023-04-12(11:13:15 +0000)

### Other

- [uspe] Remove default endpoint id for uspe

## Release v1.1.0 - 2023-03-24(09:36:44 +0000)

### New

- [USP] Add support for subscriptions to usp-endpoint

## Release v1.0.11 - 2023-02-10(10:36:34 +0000)

## Release v1.0.10 - 2023-01-11(16:23:17 +0000)

### Changes

- [KPN][USP] max_depth has no effect on the Get Message

## Release v1.0.9 - 2023-01-09(15:30:27 +0000)

### Fixes

- Handle command output args for non-backend processes

## Release v1.0.8 - 2022-11-24(09:34:27 +0000)

### Fixes

- [AMX] Apply new amxd_path_setf formatting

## Release v1.0.7 - 2022-11-24(08:37:43 +0000)

### Other

- Rename EndpointID

## Release v1.0.6 - 2022-11-23(11:54:03 +0000)

### Other

- Update IMTP defaults

## Release v1.0.5 - 2022-11-22(15:11:03 +0000)

### Other

- Opensource component

## Release v1.0.4 - 2022-11-07(16:50:18 +0000)

### Fixes

- [USP] uspi_con_read can return with value 1

## Release v1.0.3 - 2022-10-26(06:54:37 +0000)

### Changes

- USP agent/controller must indicate it is interested in USP messages

## Release v1.0.2 - 2022-10-24(09:53:45 +0000)

### Fixes

- Add extra logging
- [USP] usp-endpoint should have separate entrypoint

## Release v1.0.1 - 2022-09-27(12:11:20 +0000)

### Fixes

- [USP] usp-endpoint exits when no bus ctx is found

## Release v1.0.0 - 2022-09-05(13:04:47 +0000)

### New

- Add MTP of type IMTP

## Release v0.3.0 - 2022-06-03(06:27:02 +0000)

### New

- Implement GetInstances request and response

### Other

- ODL option import-dbg should be disabled

## Release v0.2.0 - 2022-05-03(15:17:47 +0000)

### New

- Implement operate command

## Release v0.1.6 - 2022-02-03(16:37:43 +0000)

### Fixes

- Remove impt_tlv_type_msgid

## Release v0.1.5 - 2021-12-13(11:58:45 +0000)

### Fixes

- Remove references to libmosquitto_poc
- Controller must not crash when receiving USP Error messages

### Changes

- [USP] Agent must publish responses on controller's reply-to topic

## Release v0.1.4 - 2021-09-09(12:07:27 +0000)

### Fixes

- [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt

## Release v0.1.3 - 2021-08-25(07:42:43 +0000)

### Other

- USP components should have debian packages

## Release v0.1.2 - 2021-06-01(09:09:36 +0000)

### Fixes

- Build issue on SPS3 due to missing include

## Release v0.1.1 - 2021-05-31(14:46:31 +0000)

- [USP] Integrate usp-endpoint and mod-usp-cli components

## Release v0.1.0 - 2021-05-31(09:35:12 +0000)

### New

- List USP back-end in runtime dependencies

## Release v0.0.2 - 2021-04-26(13:59:22 +0000)

### Changes

- Update mod_controller references to usp_endpoint references
- Update set RPC to new libusp API

## Release 0.0.1 - 2021-04-21(06:48:25 +0000)

### New

- Initial release
