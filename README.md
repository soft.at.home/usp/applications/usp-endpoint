# USP Endpoint as a USP agent or controller

## Introduction

This application can be used to run a basic USP agent or controller. Not all features described in the USP specification are implemented, but the default USP RPCs (get/set/...) can be handled by controller and agent.

For the controller a `LocalController` datamodel is defined with the following RPCs under the `LocalController.` object:

- `usp_get`: get a data model object or parameter based on the USP specification.
- `usp_set`: set one or more object parameters.
- `usp_add`: add a new instance of a multi-instance object.
- `usp_del`: delete an instance of a multi-instance object.
- `usp_get_supported_dm`: get the supported data model starting from a given object.
- `usp_operate`: invoke a data model operation.
- `usp_get_instances`: get the instances of a multi-instance object

These operations are executed remotely by using MQTT as an MTP to communicate with a remote USP Endpoint. How the operations can be used will be explained after the compilation and install process. In its current implementation only MQTT is supported as a communication protocol, however the code should be modular enough to allow easy integration of different MTPs.

Besides MQTT, the USP Endpoint also supports the IMTP, which will mainly be used for connecting data models inside containers with the host system. More information about the IMTP can be found in the [IMTP configuration document](doc/mtp_imtp_configuration.md).

## Building

### Build time prerequisites

- [libamxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxb](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp)
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp)
- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi)
- [libsahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)

You can install these libraries from source or using their debian packages. To install them from source, refer to their corresponding repositories for more information.
To install everything using debian packages, you can run

```bash
sudo apt update
sudo apt install sah-lib-sahtrace-dev protobuf-c-compiler libprotobuf-c-dev libamxc libamxp libamxm libamxd libamxb libamxo libimtp libusp libuspi
```

### Build and install usp-endpoint

1. Clone the git repository

    To be able to build it, you need the source code. It is recommended to follow the gitlab repository structure for your cloned repositories. Depending on which folder you mounted, your setup might be slightly different.

    ```bash
    mkdir -p ~/workspace/amx/usp/applications
    cd ~/workspace/amx/usp/applications
    git clone git@gitlab.com:soft.at.home/usp/applications/usp-endpoint.git
    ``` 

1. Build it

    When using the internal gitlab, you must define an environment variable `VERSION_PREFIX` before building.

    ```bash
    export VERSION_PREFIX="master_"
    ```

    After the variable is set, you can build the package.

    ```bash
    cd ~/workspace/amx/usp/applications/usp-endpoint
    make
    ```

1. Install it

    You can use the install target in the makefile to install the module.

    > WARNING!
    > Before you run the install target, you should note that there are 3 configuration variables for this component:
    > - CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA: install the needed files to run the agent plugin
    > - CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC: install the needed files to run the controller plugin
    > - CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE: install the needed files to run this plugin in a container as a proxy to the host system

    > All of these variables are boolean variables that are set to `n` by default. Make sure you set the ones you need to `y` before running the `install` target or you will be missing important files. You can add them to the bottom of the `makefile.inc` file for example. Alternatively you can provide the option when you run the install target, for example `sudo -E make "CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC=y" install`

    ```bash
    cd ~/workspace/amx/usp/applications/usp-endpoint
    sudo -E make install
    ```

## Using the controller and agent applications

### Run time prerequisites

- [amxrt](https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt)
- [mod-dmext](https://gitlab.com/prpl-foundation/components/core/modules/mod_dmext)
- [mod-sahtrace (optional)](https://gitlab.com/prpl-foundation/components/core/modules/mod_dmext)

The run time prerequisites for this component will depend on the MTP you wish to use. Read the [MTP MQTT configuration guidelines](doc/mtp_mqtt_configuration.md) if you want to use MQTT and read the [MTP IMTP configuration guidelines](doc/mtp_imtp_configuration.md) if you want to use the IMTP.

The generic runtime dependencies for this component are `amxrt`, `mod-dmext` and `mod-sahtrace`. They can be installed from source or from debian packages:

```bash
sudo apt update
sudo apt install -y amxrt mod-dmext mod-sahtrace
```

### Running the controller module

During the installation of the controller module 2 symbolic links to `amxrt` are created

- /usr/bin/uspa -> /usr/bin/amxrt
- /usr/bin/uspc -> /usr/bin/amxrt

You can use `uspa` to run this module as a `LocalAgent` and you can use `uspc` to run the module as a `LocalController`. The `LocalAgent` version of this component does not support all USP agent functionality as specified in TR-369 USP. It just knows how to respond to requests sent by a `LocalController`. There is another [USP agent](https://gitlab.com/soft.at.home/usp/applications/uspagent) with more functionality available, which can also be used to reply to requests from this controller. The responses will be identical to the responses from the `LocalAgent` in this repository.

> WARNING: For the remainder of this document, we will assume you use the `uspagent` linked above, because this one has more support for USP features. This one has a few other run time requirements, which are explained in [its REAMDE.md](https://gitlab.com/soft.at.home/usp/applications/uspagent#running). Make sure you install and run these dependencies as well.

### Invoking USP operations

Invoking the USP operations (get/set/...) is currently only possible when using MQTT as the MTP. However since this can easily be extended to other MTPs, we will leave the explanation in the main README file. The documentation will be updated when support for other MTPs is added.

> WARNING: Even though the IMTP has been added as an extra MTP, it does not yet support invoking these USP operations. This may be added in the future.

Follow the next steps in the correct order to send requests from the USP controller to the USP agent.

- Run the MQTT client program with `tr181-mqtt -D`
- Run the USP controller `uspc -D`
- (optional) Run the USP agent `uspagent -D` (and all of its dependencies)
- Run `pcb_cli` to invoke the desired RPCs

> NOTE: `uspagent` is listed as optional, because in many situations you will be sending requests to a remote USP agent. In this case you don't need to run an extra agent locally.

> WARNING: To set up the communication between `tr181-mqtt` and `uspc` a socket will be created under `/var/run/mqtt/`. This folder is (by default) not writable by a normal user. You will either need to create it yourself and change the ownership to your current user or run all programs as root (sudo). Furthermore, if you have the ubus backend installed, the plugins should always be run as root, because they can only connect to ubus with root privileges.

The functions that you can use are

- LocalController.usp_get(agent_eid <string>, request <htable>)
- LocalController.usp_set(agent_eid <string>, request <htable>)
- LocalController.usp_add(agent_eid <string>, request <htable>)
- LocalController.usp_del(agent_eid <string>, request <htable>)
- LocalController.usp_get_supported_dm(agent_eid <string>, request <htable>)
- LocalController.usp_operate(agent_eid <string>, request <htable>)
- LocalController.usp_get_instances(agent_eid <string>, request <htable>)

### Operation examples

An example of how to call each function will be given in this section. The `Phonebook` datamodel will we used as the test datamodel to execute all operations. Those who have followed the Ambiorix tutorials will probably have it somewhere on their system. If you don't have it yet, you can find it in [one of the tutorials](https://gitlab.com/prpl-foundation/components/ambiorix/tutorials/tools/amxrt/-/tree/main/labs/lab2).

The first argument of each operation will always be the agent EndpointID you want to communicate with. When MQTT is used, the request will be published on topic `agent/<endpoint-id>`, where endpoint-id is the value you provide. You can check the value of the agent EndpointID in the data model under `LocalAgent.EndpointID`. By default all EndpointIDs currently have the following format: `proto:Agent-<hostname>`

The `<hostname>` will be different depending on where you are running the USP agent. You can verify the hostname of your system by typing `hostname` in the terminal where you are planning to run the USP agent.

> Note: We suggest running the various processes in the background using the `-D` option from amxrt i.e. `tr181-mqtt -D`, `uspc -D`, `uspagent -D`. This allows you to run everything in a single terminal. To get the responses in `pcb_cli`, you can subscribe to the LocalController object with the following command `LocalController.?&`. You can also see the responses in the trace logs if they have been enabled.

Trace logs can be enabled via `pcb-cli` or `ubus-cli`. You can first take a look at what has already been configured for the agent (Discovery data model) and controller (LocalController data model)

```bash
root - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > LocalController.list_trace_zones()
LocalController.list_trace_zones() returned
[
    {
        mtp_mqtt = 500,
        usp = 500,
        uspcm = 200,
        uspe = 500,
        uspm = 500
    }
]

root - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > Discovery.list_trace_zones()
Discovery.list_trace_zones() returned
[
    {
        imtp = 200,
        uspagent = 500,
        uspl_trace = 500
    }
]
```

In the example above several trace zones already have log level 500. If this is not the case, the log level can be updated with `set_trace_zone`, for example

```bash
root - pcb:/var/run/pcb_sys - [pcb-cli] (0)
 > Discovery.set_trace_zone(zone = uspl_trace, level = 500)
Discovery.set_trace_zone() returned
[
    <NULL>
]
```

#### Adding an instance

Using pcb_cli call the following function with an updated hostname for your system

```bash
LocalController.usp_add("proto::Agent-a9bf597211ac",{allow_partial: true, requests: [{object_path: "Phonebook.Contact.", parameters: [{param: "FirstName", value: "John", required: false}, {param: "LastName", value: "Doe", required: false}]}]})
```

This should result in the following USP messages, which can be seen in the logs if the `uspl_trace` log is set to 500:

```text
header {
  msg_id: "16"
  msg_type: ADD
}
body {
  request {
    add {
      allow_partial: true
      create_objs {
        obj_path: "Phonebook.Contact."
        param_settings {
          param: "FirstName"
          value: "John"
          required: false
        }
        param_settings {
          param: "LastName"
          value: "Doe"
          required: false
        }
      }
    }
  }
}
header {
  msg_id: "16"
  msg_type: ADD_RESP
}
body {
  response {
    add_resp {
      created_obj_results {
        requested_path: "Phonebook.Contact."
        oper_status {
          oper_success {
            instantiated_path: "Phonebook.Contact.1."
          }
        }
      }
    }
  }
}
```

> NOTE: If the instances have key parameters, they will also be shown here. For the Phonebook data model, this is not the case.
> NOTE: It is recommended to set allow_partial to true, because we don't support a rollback mechanism for add requests that partially fail.

#### Getting an object

Using pcb_cli call the following function with an updated hostname for your system

```bash
LocalController.usp_get("proto::Agent-a9bf597211ac",{paths: ["Phonebook."], max_depth: 0})
```

Note that a `max_depth` of 0 means the complete tree must be returned, while a `max_depth` of 1 will correspond with an ambiorix depth of 0. If the `max_depth` field is missing, it will default to 0, so the complete tree will also be returned.

You should see the request and response in the sahtrace logs.

```text
header {
  msg_id: "17"
  msg_type: GET
}
body {
  request {
    get {
      param_paths: "Phonebook."
      max_depth: 0
    }
  }
}
header {
  msg_id: "17"
  msg_type: GET_RESP
}
body {
  response {
    get_resp {
      req_path_results {
        requested_path: "Phonebook."
        err_code: 0
        resolved_path_results {
          resolved_path: "Phonebook."
        }
        resolved_path_results {
          resolved_path: "Phonebook.Contact.1."
          result_params {
            key: "LastName"
            value: "Doe"
          }
          result_params {
            key: "FirstName"
            value: "John"
          }
        }
      }
    }
  }
}
```

#### Setting new parameters

Using pcb_cli call the following function with an updated hostname for your system

```bash
LocalController.usp_set("proto::Agent-a9bf597211ac",{allow_partial: true, requests: [{object_path: "Phonebook.Contact.1.", parameters: [{ param: "FirstName", value: "Doe", required: true }, { param: "LastName", value: "John", required: true }]}]})
```

You should see the request and response in the sahtrace logs.

```text
header {
  msg_id: "18"
  msg_type: SET
}
body {
  request {
    set {
      allow_partial: true
      update_objs {
        obj_path: "Phonebook.Contact.1."
        param_settings {
          param: "FirstName"
          value: "Doe"
          required: true
        }
        param_settings {
          param: "LastName"
          value: "John"
          required: true
        }
      }
    }
  }
}
header {
  msg_id: "18"
  msg_type: SET_RESP
}
body {
  response {
    set_resp {
      updated_obj_results {
        requested_path: "Phonebook.Contact.1."
        oper_status {
          oper_success {
            updated_inst_results {
              affected_path: "Phonebook.Contact.1."
              updated_params {
                key: "LastName"
                value: "John"
              }
              updated_params {
                key: "FirstName"
                value: "Doe"
              }
            }
          }
        }
      }
    }
  }
}
```

#### Sending a get instances message

Using pcb_cli call the `usp_get_instances` function with an updated hostname for your system

```bash
LocalController.usp_get_instances("proto::Agent-a9bf597211ac",{first_level_only: true, obj_paths: ["Phonebook.Contact."]})
```

You should see the request and response in the sahtrace logs.

```
header {
  msg_id: "23"
  msg_type: GET_INSTANCES
}
body {
  request {
    get_instances {
      obj_paths: "Phonebook.Contact."
      first_level_only: true
    }
  }
}
header {
  msg_id: "23"
  msg_type: GET_INSTANCES_RESP
}
body {
  response {
    get_instances_resp {
      req_path_results {
        requested_path: "Phonebook.Contact."
        err_code: 0
        curr_insts {
          instantiated_obj_path: "Phonebook.Contact.1."
        }
      }
    }
  }
}
```

> NOTE: If the instances have key parameters, they will also be shown here. For the Phonebook data model, this is not the case.

#### Deleting an instance

Using pcb_cli call the following function with an updated hostname for your system

```bash
LocalController.usp_del("proto::Agent-711f9935704d",{allow_partial: true, requests: ["Phonebook.Contact.1."]})
```

You should see the request and response in the sahtrace logs.

```text
header {
  msg_id: "19"
  msg_type: DELETE
}
body {
  request {
    delete {
      allow_partial: true
      obj_paths: "Phonebook.Contact.1."
    }
  }
}
header {
  msg_id: "19"
  msg_type: DELETE_RESP
}
body {
  response {
    delete_resp {
      deleted_obj_results {
        requested_path: "Phonebook.Contact.1."
        oper_status {
          oper_success {
            affected_paths: "Phonebook.Contact.1."
            affected_paths: "Phonebook.Contact.1.PhoneNumber."
            affected_paths: "Phonebook.Contact.1.E-Mail."
          }
        }
      }
    }
  }
}
```

> NOTE: It is recommended to set allow_partial to true, because we don't support a rollback mechanism for delete requests that partially fail.

#### Getting the supported data model

Using pcb_cli call the following function with an updated hostname for your system

```bash
LocalController.usp_get_supported_dm("proto::Agent-711f9935704d",{flags: {first_level_only: true, return_commands: false, return_events: false, return_params: false}, paths: ["Phonebook."]})
```

You should see the request and response in the sahtrace logs.

```text
header {
  msg_id: "20"
  msg_type: GET_SUPPORTED_DM
}
body {
  request {
    get_supported_dm {
      obj_paths: "Phonebook."
      first_level_only: true
      return_commands: false
      return_events: false
      return_params: false
      return_unique_key_sets: false
    }
  }
}
header {
  msg_id: "20"
  msg_type: GET_SUPPORTED_DM_RESP
}
body {
  response {
    get_supported_dm_resp {
      req_obj_results {
        req_obj_path: "Phonebook."
        err_code: 0
        err_msg: ""
        data_model_inst_uri: "urn:broadband-forum-org:tr-181-2-16-0"
        supported_objs {
          supported_obj_path: "Phonebook."
          access: OBJ_READ_ONLY
          is_multi_instance: false
        }
        supported_objs {
          supported_obj_path: "Phonebook.Contact.{i}."
          access: OBJ_ADD_DELETE
          is_multi_instance: true
        }
      }
    }
  }
}
```

#### Sending an operate message

Using pcb_cli call the `usp_operate` function with an updated hostname for your system

```bash
LocalController.usp_operate("proto::Agent-a9bf597211ac",{command:"Greeter.say",command_key:"dummy",send_resp: true, input_args: {from:"controller",message:"hello"}})
```

You should see the request and response in the sahtrace logs.

```text
header {
  msg_id: "29"
  msg_type: OPERATE
}
body {
  request {
    operate {
      command: "Greeter.say"
      command_key: "dummy"
      send_resp: true
      input_args {
        key: "message"
        value: "hello"
      }
      input_args {
        key: "from"
        value: "controller"
      }
    }
  }
}
header {
  msg_id: "29"
  msg_type: OPERATE_RESP
}
body {
  response {
    operate_resp {
      operation_results {
        executed_command: "Greeter.say"
        req_output_args {
          output_args {
            key: "_retval"
            value: "hello"
          }
        }
      }
    }
  }
}
```

> NOTE: Since the `phonebook` plugin doesn't have methods to call, we used the [Greeter plugin](https://gitlab.com/prpl-foundation/components/ambiorix/examples/datamodel/greeter_plugin) instead in this example.
> NOTE: Make sure that Greeter.State is first set to 'Start' and that you have the right ACLs to call the `say()` method.

### config options

TODO

### How to add diferent MTPs

TODO
