#!/bin/sh

case $1 in
    start|boot)
	uspa -D
        ;;
    stop)
        if [ -f /var/run/uspa.pid ]; then
            kill `cat /var/run/uspa.pid`
        fi
        ;;
    debuginfo)
	echo "TODO debuginfo"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log USP Agent"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac