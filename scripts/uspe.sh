#!/bin/sh

case $1 in
    start|boot)
    ! mkdir /var/run/usp/
	uspe -D
        ;;
    stop)
        if [ -f /var/run/uspe.pid ]; then
            kill `cat /var/run/uspe.pid`
        fi
        ;;
    debuginfo)
	echo "TODO debuginfo"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log USP Endpoint"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac