#!/bin/sh

case $1 in
    start|boot)
	uspc -D
        ;;
    stop)
        if [ -f /var/run/uspc.pid ]; then
            kill `cat /var/run/uspc.pid`
        fi
        ;;
    debuginfo)
	echo "TODO debuginfo"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
	echo "TODO log USP Controller"
	;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac