include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean

install: all
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/controller/uspc.odl $(DEST)/etc/amx/uspc/uspc.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/controller/uspc_defaults.odl $(DEST)/etc/amx/uspc/defaults/odl/01-uspc_defaults.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/controller/uspc_definition.odl $(DEST)/etc/amx/uspc/uspc_definition.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 odl/agent/uspa.odl $(DEST)/etc/amx/uspa/uspa.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 odl/agent/uspa_defaults.odl $(DEST)/etc/amx/uspa/defaults/odl/01-uspa_defaults.odl
endif
ifeq ($(or $(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),$(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE)),y)
	$(INSTALL) -D -p -m 0644 odl/agent/uspa_definition.odl $(DEST)/etc/amx/uspa/uspa_definition.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0644 odl/endpoint/uspe.odl $(DEST)/etc/amx/uspe/uspe.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0644 odl/endpoint/uspe_defaults.odl $(DEST)/etc/amx/uspe/defaults/odl/01-uspe_defaults.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0644 odl/endpoint/uspe_definition.odl $(DEST)/etc/amx/uspe/uspe_definition.odl
endif
	$(INSTALL) -D -p -m 0644 odl/mtp/mqtt/mtp_mqtt.odl $(DEST)/etc/amx/usp/mtp_mqtt.odl
	$(INSTALL) -D -p -m 0644 odl/mtp/uds/mtp_uds.odl $(DEST)/etc/amx/usp/mtp_uds.odl
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 odl/mtp/mqtt/mqtt-client-uspa.odl $(DEST)/etc/amx/tr181-mqtt/defaults/odl/01-mqtt-client-uspa.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/mtp/mqtt/mqtt-client-uspc.odl $(DEST)/etc/amx/tr181-mqtt/defaults/odl/01-mqtt-client-uspc.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 config/uci/MQTT-uspa $(DEST)/etc/amx/tr181-mqtt/defaults/uci/MQTT-01-uspa
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 config/uci/MQTT-uspc $(DEST)/etc/amx/tr181-mqtt/defaults/uci/MQTT-01-uspc
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)$(BINDIR)/uspc
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)$(BINDIR)/uspa
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)$(BINDIR)/uspe
endif
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/modules/$(COMPONENT).so
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0755 scripts/uspa.sh $(DEST)$(INITDIR)/uspa
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0755 scripts/uspc.sh $(DEST)$(INITDIR)/uspc
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0755 scripts/uspe.sh $(DEST)$(INITDIR)/uspe
endif

package: all
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/controller/uspc.odl $(PKGDIR)/etc/amx/uspc/uspc.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/controller/uspc_defaults.odl $(PKGDIR)/etc/amx/uspc/defaults/odl/01-uspc_defaults.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/controller/uspc_definition.odl $(PKGDIR)/etc/amx/uspc/uspc_definition.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 odl/agent/uspa.odl $(PKGDIR)/etc/amx/uspa/uspa.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 odl/agent/uspa_defaults.odl $(PKGDIR)/etc/amx/uspa/defaults/odl/01-uspa_defaults.odl
endif
ifeq ($(or $(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),$(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE)),y)
	$(INSTALL) -D -p -m 0644 odl/agent/uspa_definition.odl $(PKGDIR)/etc/amx/uspa/uspa_definition.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0644 odl/endpoint/uspe.odl $(PKGDIR)/etc/amx/uspe/uspe.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0644 odl/endpoint/uspe_defaults.odl $(PKGDIR)/etc/amx/uspe/defaults/odl/01-uspe_defaults.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0644 odl/endpoint/uspe_definition.odl $(PKGDIR)/etc/amx/uspe/uspe_definition.odl
endif
	$(INSTALL) -D -p -m 0644 odl/mtp/mqtt/mtp_mqtt.odl $(PKGDIR)/etc/amx/usp/mtp_mqtt.odl
	$(INSTALL) -D -p -m 0644 odl/mtp/uds/mtp_uds.odl $(PKGDIR)/etc/amx/usp/mtp_uds.odl
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 odl/mtp/mqtt/mqtt-client-uspa.odl $(PKGDIR)/etc/amx/tr181-mqtt/defaults/odl/01-mqtt-client-uspa.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 odl/mtp/mqtt/mqtt-client-uspc.odl $(PKGDIR)/etc/amx/tr181-mqtt/defaults/odl/01-mqtt-client-uspc.odl
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0644 config/uci/MQTT-uspa $(PKGDIR)/etc/amx/tr181-mqtt/defaults/uci/MQTT-01-uspa
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0644 config/uci/MQTT-uspc $(PKGDIR)/etc/amx/tr181-mqtt/defaults/uci/MQTT-01-uspc
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/uspc
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)$(BINDIR)/uspc
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/uspa
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)$(BINDIR)/uspa
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/uspe
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)$(BINDIR)/uspe
endif
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/modules/$(COMPONENT).so
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(INSTALL) -D -p -m 0755 scripts/uspa.sh $(PKGDIR)$(INITDIR)/uspa
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(INSTALL) -D -p -m 0755 scripts/uspc.sh $(PKGDIR)$(INITDIR)/uspc
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(INSTALL) -D -p -m 0755 scripts/uspe.sh $(PKGDIR)$(INITDIR)/uspe
endif
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(eval ODLFILES += odl/controller/uspc.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(eval ODLFILES += odl/controller/uspc_defaults.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(eval ODLFILES += odl/controller/uspc_definition.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(eval ODLFILES += odl/agent/uspa.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(eval ODLFILES += odl/agent/uspa_defaults.odl)
endif
ifeq ($(or $(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),$(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE)),y)
	$(eval ODLFILES += odl/agent/uspa_definition.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(eval ODLFILES += odl/endpoint/uspe.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(eval ODLFILES += odl/endpoint/uspe_defaults.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPE),y)
	$(eval ODLFILES += odl/endpoint/uspe_definition.odl)
endif
	$(eval ODLFILES += odl/mtp/mqtt/mtp_mqtt.odl)
	$(eval ODLFILES += odl/mtp/uds/mtp_uds.odl)
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPA),y)
	$(eval ODLFILES += odl/mtp/mqtt/mqtt-client-uspa.odl)
endif
ifeq ($(CONFIG_SAH_SERVICES_USP-ENDPOINT_INSTALL_USPC),y)
	$(eval ODLFILES += odl/mtp/mqtt/mqtt-client-uspc.odl)
endif

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

.PHONY: all clean changelog install package doc