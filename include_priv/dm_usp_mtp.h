/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DM_USP_MTP_H__)
#define __DM_USP_MTP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "usp_main.h"

// Data model MTP utility functions - change values in the data model
amxd_object_t * PRIVATE dm_mtp_get_protocol_object(amxd_object_t* mtp);

int PRIVATE dm_mtp_set_status(amxd_object_t* mtp,
                              const char* status,
                              const char* msg);

int PRIVATE dm_mtp_enable(amxd_object_t* mtp, bool enable);
bool PRIVATE dm_mtp_is_up(amxd_object_t* mtp);

// MTP Enable/Disable handler functions - called from event callback functions
int PRIVATE usp_mtp_enable(UNUSED amxd_object_t* mtps,
                           amxd_object_t* mtp,
                           UNUSED void* priv);

int PRIVATE usp_mtp_disable(UNUSED amxd_object_t* mtps,
                            UNUSED amxd_object_t* mtp,
                            UNUSED void* priv);

// MTP Action callbacks - bound to objects in odl file
amxd_status_t _lc_mtp_mqtt_proto_cleanup(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv);

// MTP event callback functions - bound to events in odl file
void _usp_mtp_added(const char* const sig_name,
                    const amxc_var_t* const data,
                    void* const priv);

void _usp_mtp_toggle(const char* const sig_name,
                     const amxc_var_t* const data,
                     void* const priv);

void _usp_mtp_connection_closed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv);
#ifdef __cplusplus
}
#endif

#endif // __DM_USP_MTP_H__
