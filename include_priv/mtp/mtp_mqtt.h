/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__MTP_MQTT_H__)
#define __MTP_MQTT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "usp.h"

#include <imtp/imtp_message.h>
#include <imtp/imtp_connection.h>
#include <usp/uspl.h>
#include <uspi/uspi_connection.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxo/amxo.h>
#include <amxo/amxo_save.h>
#include <amxb/amxb.h>
#include <amxm/amxm.h>

// mtp MQTT - utility functions
amxd_dm_t * PRIVATE mtp_mqtt_get_dm(void);
amxb_bus_ctx_t* PRIVATE mtp_mqtt_get_bus_ctx(void);
amxo_parser_t* mtp_mqtt_get_parser(void);

// MQTT Management - remote data model
int PRIVATE mqtt_start(const char* client);
int PRIVATE mqtt_stop(const char* client);

int PRIVATE mqtt_add_subscription(const char* client, const char* topic);
int PRIVATE mqtt_del_subscription(const char* client, const char* topic);

int PRIVATE mqtt_subscribe(const char* client, amxp_slot_fn_t cb, void* priv);
int PRIVATE mqtt_unsubscribe(const char* client, amxp_slot_fn_t cb, void* priv);

// imtp mqtt connection functions
int PRIVATE imtp_mqtt_connect(uspi_con_t** con,
                              amxc_var_t* config,
                              const char** msg,
                              void* priv);

int PRIVATE imtp_mqtt_disconnect(uspi_con_t** con,
                                 amxc_var_t* config);

int mtp_mqtt_imtp_send(uspi_con_t* con,
                       uspl_tx_t* usp_tx,
                       const char* eid,
                       const char* type,
                       const char* notif_topic,
                       amxd_object_t* mqtt_obj);

// mtp MQTT entry point
int _mtp_mqtt_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);

// utility functions
int PRIVATE mtp_mqtt_configure_topics(amxd_object_t* mqtt);

// event callback functions - not data model related
void PRIVATE mtp_mqtt_connection_deleted(const char* const sig_name,
                                         const amxc_var_t* const data,
                                         void* const priv);

void PRIVATE mtp_mqtt_disconnected(const char* const sig_name,
                                   const amxc_var_t* const data,
                                   void* const priv);

// Data Model event callback functions
void _mtp_mqtt_init(const char* const sig_name,
                    const amxc_var_t* const data,
                    void* const priv);

void _mtp_mqtt_update_all(const char* const sig_name,
                          const amxc_var_t* const data,
                          void* const priv);

void _mtp_mqtt_update_subs(const char* const sig_name,
                           const amxc_var_t* const data,
                           void* const priv);

// Data Model action callback functions
amxd_status_t _mtp_mqtt_cleanup(amxd_object_t* object,
                                amxd_param_t* param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv);

#ifdef __cplusplus
}
#endif

#endif // __MTP_MQTT_H__
