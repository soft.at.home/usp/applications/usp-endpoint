/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <debug/sahtrace.h>

#include "dm_usp.h"
#include "dm_usp_mtp.h"

#define ME "uspe"

static int usp_mtp_start(amxd_object_t* mtps,
                         amxd_object_t* mtp,
                         void* priv) {
    amxc_var_t args;
    amxc_var_t ret;
    char* protocol_name = amxd_object_get_value(cstring_t, mtp, "Protocol", NULL);
    char* path = amxd_object_get_path(mtp, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
    bool mtp_is_enabled = amxd_object_get_value(bool, mtp, "Enable", NULL);
    bool* enable = (bool*) priv;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);

    SAH_TRACEZ_INFO(ME, "Start MTP %s", path);

    if(amxm_has_function(NULL, protocol_name, "init")) {
        SAH_TRACEZ_INFO(ME, "=> call init");
        amxc_var_add_key(cstring_t, &args, "path", path);
        if(amxm_execute_function(NULL, protocol_name, "init", &args, &ret) != 0) {
            SAH_TRACEZ_ERROR(ME, "=> init failed");
        }
    }

    if(*enable && mtp_is_enabled) {
        SAH_TRACEZ_INFO(ME, "=> Enable");
        usp_mtp_enable(mtps, mtp, NULL);
    }

    SAH_TRACEZ_INFO(ME, "=> MTP Configured and ready");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(protocol_name);
    free(path);

    return 0;
}

void _usp_start(UNUSED const char* const sig_name,
                UNUSED const amxc_var_t* const data,
                UNUSED void* const priv) {
    amxd_object_t* usp_obj = amxd_dm_findf(usp_get_dm(), "Local%s.", usp_get_type());
    amxd_object_t* mtps = amxd_object_get(usp_obj, "MTP");
    bool enable = amxd_object_get_value(bool, usp_obj, "Enable", NULL);

    SAH_TRACEZ_INFO(ME, "Start MTPs");
    amxd_object_for_all(mtps, "*.", usp_mtp_start, &enable);
}

// Event callback function - set in odl file
//    on event "dm:object-changed" call usp_toggle
//             filter 'path == "LocalController." &&
//             contains("parameters.Enable")';
void _usp_toggle(UNUSED const char* const sig_name,
                 const amxc_var_t* const data,
                 UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Local%s Enable toggeled", usp_get_type());
    amxd_dm_t* dm = usp_get_dm();
    amxd_object_t* lc = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* mtps = amxd_object_findf(lc, "MTP.");
    bool enable = GETP_BOOL(data, "parameters.Enable.to");

    const char* filter = "[Status == 'Up'].";
    amxd_instance_cb_t fn = usp_mtp_disable;

    if(enable) {
        fn = usp_mtp_enable;
        filter = "[Enable == true].";
    }

    amxd_object_for_all(mtps, filter, fn, NULL);
}
