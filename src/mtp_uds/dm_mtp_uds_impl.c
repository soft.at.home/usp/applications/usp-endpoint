/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <uspi/uspi_connection.h>
#include <uspi/uspi_message.h>
#include <uspi/uspi_utils.h>

#include "usp_main.h"
#include "mtp_utils.h"
#include "mtp/mtp_uds.h"
#include "agent/dm_controller.h"

#define ME "mtp_uds"
#define BROKER_AGENT_SOCK "/var/run/imtp/broker_agent_path"
#define BROKER_CONTROLLER_SOCK "/var/run/imtp/broker_controller_path"
#define ENDPOINT_AGENT_SOCK "/var/run/usp/endpoint_agent_path"
#define ENDPOINT_CONTROLLER_SOCK "/var/run/usp/endpoint_controller_path"

static void mtp_uds_connection_remove(uspi_con_t* con) {
    amxo_parser_t* parser = mtp_uds_get_parser();

    when_null(con, exit);

    if((con->flags & USPI_CON_LISTEN) == USPI_CON_LISTEN) {
        amxc_llist_for_each(it, &con->accepted_cons) {
            uspi_con_t* con_accepted = amxc_container_of(it, uspi_con_t, lit);
            amxo_connection_remove(parser, uspi_con_get_fd(con_accepted));
        }
    }

    amxo_connection_remove(parser, uspi_con_get_fd(con));
    uspi_con_disconnect(&con);

exit:
    return;
}

static int mtp_uds_disconnect(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "MTP UDS disconnect");
    const char* mtp_path = GET_CHAR(args, "MTP");
    amxd_object_t* mtp_object = amxd_dm_findf(mtp_uds_get_dm(), "%s", mtp_path);
    amxd_object_t* uds_obj = amxd_object_get(mtp_object, "UDS");
    usp_connections_t* conns = NULL;

    when_null(uds_obj, exit);

    conns = (usp_connections_t*) uds_obj->priv;
    when_null(conns, exit);

    mtp_uds_connection_remove(conns->con_agent);
    mtp_uds_connection_remove(conns->con_contr);
    free(conns);
    uds_obj->priv = NULL;

exit:
    return 0;
}

static amxd_object_t* mtp_uds_obj_from_accepted_con(uspi_con_t* con) {
    amxd_object_t* mtp = NULL;
    uspi_con_t* listen_con;

    when_null(con, exit);
    when_false((con->flags & USPI_CON_ACCEPT) == USPI_CON_ACCEPT, exit);

    listen_con = amxc_container_of(con->lit.llist, uspi_con_t, accepted_cons);
    when_null(listen_con, exit);
    mtp = (amxd_object_t*) listen_con->priv;

exit:
    return mtp;
}

static void mtp_uds_handle_request(uspi_con_t* con, uspi_msg_t* msg) {
    amxc_var_t data;
    amxc_var_t* request_data = NULL;
    amxd_object_t* uds_obj = NULL;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    request_data = amxc_var_add_key(amxc_htable_t, &data, "data", NULL);
    amxc_var_move(request_data, msg->request);

    if((con->flags & USPI_CON_ACCEPT) == USPI_CON_ACCEPT) {
        uds_obj = mtp_uds_obj_from_accepted_con(con);
    } else {
        uds_obj = (amxd_object_t*) con->priv;
    }

    if(uds_obj == NULL) {
        SAH_TRACEZ_ERROR(ME, "Can't find MTP that corresponds with con");
        goto exit;
    }

    mtp_utils_handle_request(uds_obj, &data, msg->usp_rx);

exit:
    amxc_var_clean(&data);
}

static int mtp_uds_handle_protobuf(uspi_con_t* con, const imtp_tlv_t* tlv_protobuf) {
    int retval = -1;
    uspi_msg_t* msg = NULL;
    bool is_agent = (con->flags & USPI_CON_AGENT) == USPI_CON_AGENT;
    uint32_t con_type = (is_agent ? USPI_CON_AGENT : USPI_CON_CONTROLLER);

    retval = uspi_message_extract(&msg, tlv_protobuf->value + tlv_protobuf->offset,
                                  tlv_protobuf->length);
    when_failed(retval, exit);

    if(!uspi_utils_check_sender(msg->inner_type, con_type)) {
        SAH_TRACEZ_ERROR(ME, "Received %s message from %s, ignoring",
                         is_agent ? "controller" : "agent",
                         is_agent ? "agent" : "controller");
        goto exit;
    }

    if(msg->outer_type == uspi_msg_type_error) {
        // TODO
    } else if(msg->outer_type == uspi_msg_type_request) {
        mtp_uds_handle_request(con, msg);
    } else if(msg->outer_type == uspi_msg_type_response) {
        // Case will not be reached, because all requests are handled synchronously
        // Might change in the future
    } else {
        SAH_TRACEZ_ERROR(ME, "Invalid message received");
        goto exit;
    }

    retval = 0;
exit:
    uspi_message_delete(&msg);
    return retval;
}

static int mtp_uds_handle_eid(uspi_con_t* con, const imtp_tlv_t* tlv_handshake) {
    int retval = -1;

    retval = uspi_con_add_eid(con, tlv_handshake);
    when_failed(retval, exit);
    SAH_TRACEZ_INFO(ME, "Add eid '%s' to con with fd %d", con->eid, uspi_con_get_fd(con));

    if((con->flags & USPI_CON_ACCEPT) == USPI_CON_ACCEPT) {
        char* eid = amxd_object_get_value(cstring_t, usp_get_object(), "EndpointID", NULL);
        retval = uspi_con_send_eid(con, eid);
        free(eid);
        when_failed(retval, exit);

        // Add controller with EndpointID if there isn't one yet
        if(!dm_controller_exists(con->eid)) {
            dm_controller_add_inst(con->eid);
            dm_controller_add_mtp_uds(con->eid);
        }
    }

exit:
    return retval;
}

static void mtp_uds_handle_sub(uspi_con_t* con, const imtp_tlv_t* tlv_sub) {
    amxb_bus_ctx_t* ctx = usp_get_bus_ctx();
    char* pbuf = (char*) tlv_sub->value + tlv_sub->offset;
    char* recipient = dm_controller_get_instance_from_eid(con->eid);
    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    SAH_TRACEZ_INFO(ME, "Handle TLV of type subscribe for path %s", pbuf);
    when_null(recipient, exit);

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "ReferenceList", pbuf);
    amxc_var_add_key(cstring_t, &values, "Recipient", recipient);
    amxc_var_add_key(cstring_t, &values, "NotifType", "AmxNotification");
    amxc_var_add_key(bool, &values, "Enable", true);

    if(amxb_add(ctx, "LocalAgent.Subscription.", 0, NULL, &values, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add subscription for path %s", pbuf);
    }

exit:
    free(recipient);
    amxc_var_clean(&ret);
    amxc_var_clean(&values);
}

static void mtp_uds_handle_unsub(uspi_con_t* con, const imtp_tlv_t* tlv_unsub) {
    amxb_bus_ctx_t* ctx = usp_get_bus_ctx();
    char* pbuf = (char*) tlv_unsub->value + tlv_unsub->offset;
    char* recipient = dm_controller_get_instance_from_eid(con->eid);
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    SAH_TRACEZ_INFO(ME, "Handle TLV of type subscribe for path %s", pbuf);
    when_null(recipient, exit);

    amxc_string_setf(&path, "LocalAgent.Subscription.[Recipient=='%s' && ReferenceList=='%s'].",
                     recipient, pbuf);

    if(amxb_del(ctx, amxc_string_get(&path, 0), 0, NULL, &ret, 5) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to del subscription for path %s", amxc_string_get(&path, 0));
    }

exit:
    free(recipient);
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
}

static int mtp_uds_handle_msg(uspi_con_t* con, imtp_frame_t* frame) {
    int retval = -1;
    const imtp_tlv_t* tlv_protobuf = NULL;
    const imtp_tlv_t* tlv_handshake = NULL;
    const imtp_tlv_t* tlv_sub = NULL;
    const imtp_tlv_t* tlv_unsub = NULL;

    tlv_handshake = imtp_frame_get_first_tlv(frame, imtp_tlv_type_handshake);
    if(tlv_handshake != NULL) {
        mtp_uds_handle_eid(con, tlv_handshake);
    } else if((tlv_handshake == NULL) && (con->eid == NULL)) {
        SAH_TRACEZ_WARNING(ME, "Received usp message from connection with unknown EID");
        goto exit;
    }
    retval = 0;

    tlv_protobuf = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    if(tlv_protobuf != NULL) {
        retval = mtp_uds_handle_protobuf(con, tlv_protobuf);
    }

    tlv_sub = imtp_frame_get_first_tlv(frame, imtp_tlv_type_subscribe);
    if(tlv_sub != NULL) {
        mtp_uds_handle_sub(con, tlv_sub);
    }

    tlv_unsub = imtp_frame_get_first_tlv(frame, imtp_tlv_type_unsubscribe);
    if(tlv_unsub != NULL) {
        mtp_uds_handle_unsub(con, tlv_unsub);
    }

exit:
    return retval;
}

static void mtp_uds_con_read(int fd, void* priv) {
    int retval = -1;
    uspi_con_t* con = (uspi_con_t*) priv;
    imtp_frame_t* frame = NULL;
    SAH_TRACEZ_INFO(ME, "Incoming message on fd: [%d] from %s with eid %s", fd,
                    (con->flags & USPI_CON_AGENT) == USPI_CON_AGENT ? "agent" : "controller",
                    con->eid);

    retval = uspi_con_read_frame(con, &frame);
    when_failed(retval, exit);

    mtp_uds_handle_msg(con, frame);

    retval = 0;
exit:
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error reading data from socket, removing %s connection",
                         (con->flags & USPI_CON_CONNECT) == USPI_CON_CONNECT ? "connected" : "accepted");
        mtp_uds_connection_remove(con);
    }
    imtp_frame_delete(&frame);
    return;
}

static int mtp_uds_connect(amxd_object_t* uds_obj) {
    int retval = -1;
    amxo_parser_t* parser = mtp_uds_get_parser();
    usp_connections_t* conns = NULL;
    char* broker_agent = getenv("BROKER_AGENT_SOCK");
    char* broker_contr = getenv("BROKER_CONTROLLER_SOCK");
    amxc_string_t uri;

    amxc_string_init(&uri, 0);

    conns = (usp_connections_t*) calloc(1, sizeof(usp_connections_t));
    when_null(conns, exit);

    amxc_string_setf(&uri, "usp:%s", broker_agent != NULL ? broker_agent : BROKER_AGENT_SOCK);
    SAH_TRACEZ_INFO(ME, "Connecting as controller to uri: %s", amxc_string_get(&uri, 0));

    // Use con_agent, because other side is an agent and we will search for the host agent later on
    retval = uspi_con_connect(&conns->con_agent, amxc_string_get(&uri, 0));
    when_failed(retval, exit);
    uspi_con_flag_toggle(conns->con_agent, USPI_CON_AGENT); // Endpoint on the other side is an agent
    amxo_connection_add(parser, uspi_con_get_fd(conns->con_agent), mtp_uds_con_read,
                        NULL, AMXO_CUSTOM, conns->con_agent);

    amxc_string_setf(&uri, "usp:%s", broker_contr != NULL ? broker_contr : BROKER_CONTROLLER_SOCK);
    SAH_TRACEZ_INFO(ME, "Connecting as agent to uri: %s", amxc_string_get(&uri, 0));
    retval = uspi_con_connect(&conns->con_contr, amxc_string_get(&uri, 0));
    when_failed(retval, exit);
    uspi_con_flag_toggle(conns->con_contr, USPI_CON_CONTROLLER); // Endpoint on the other side is a controller
    amxo_connection_add(parser, uspi_con_get_fd(conns->con_contr), mtp_uds_con_read,
                        NULL, AMXO_CUSTOM, conns->con_contr);

    uds_obj->priv = conns;
    conns->object = uds_obj; // conns->con_agent->priv ?
    conns->con_agent->priv = uds_obj;
    conns->con_contr->priv = uds_obj;

exit:
    amxc_string_clean(&uri);
    if(retval != 0) {
        free(conns);
        uds_obj->priv = NULL;
    }
    return retval;
}

static void mtp_uds_con_accept(UNUSED int fd, void* priv) {
    int retval = -1;
    amxo_parser_t* parser = mtp_uds_get_parser();
    uspi_con_t* con_listen = (uspi_con_t*) priv;
    uspi_con_t* con_accepted = NULL;

    retval = uspi_con_accept(con_listen, &con_accepted);
    when_failed(retval, exit);

    if(uspi_con_flag_is_set(con_listen, USPI_CON_AGENT)) {
        uspi_con_flag_toggle(con_accepted, USPI_CON_CONTROLLER);
    } else {
        uspi_con_flag_toggle(con_accepted, USPI_CON_AGENT);
    }

    amxo_connection_add(parser, uspi_con_get_fd(con_accepted), mtp_uds_con_read,
                        NULL, AMXO_CUSTOM, con_accepted);

exit:
    return;
}

static int mtp_uds_listen(amxd_object_t* uds_obj) {
    int retval = -1;
    amxo_parser_t* parser = mtp_uds_get_parser();
    usp_connections_t* conns = NULL;
    amxc_string_t uri;

    amxc_string_init(&uri, 0);

    conns = (usp_connections_t*) calloc(1, sizeof(usp_connections_t));
    when_null(conns, exit);

    amxc_string_setf(&uri, "usp:%s", ENDPOINT_AGENT_SOCK);
    SAH_TRACEZ_INFO(ME, "Listening for connections on uri: %s", amxc_string_get(&uri, 0));
    retval = uspi_con_listen(&conns->con_agent, amxc_string_get(&uri, 0));
    when_failed(retval, exit);
    uspi_con_flag_toggle(conns->con_agent, USPI_CON_AGENT);
    amxo_connection_add(parser, uspi_con_get_fd(conns->con_agent), mtp_uds_con_accept,
                        amxc_string_get(&uri, 0), AMXO_CUSTOM, conns->con_agent);

    amxc_string_setf(&uri, "usp:%s", ENDPOINT_CONTROLLER_SOCK);
    SAH_TRACEZ_INFO(ME, "Listening for connections on uri: %s", amxc_string_get(&uri, 0));
    retval = uspi_con_listen(&conns->con_contr, amxc_string_get(&uri, 0));
    when_failed(retval, exit);
    uspi_con_flag_toggle(conns->con_contr, USPI_CON_CONTROLLER);
    amxo_connection_add(parser, uspi_con_get_fd(conns->con_contr), mtp_uds_con_accept,
                        amxc_string_get(&uri, 0), AMXO_CUSTOM, conns->con_contr);

    uds_obj->priv = conns;
    conns->object = uds_obj;
    conns->con_agent->priv = uds_obj;
    conns->con_contr->priv = uds_obj;

    retval = 0;
exit:
    amxc_string_clean(&uri);
    return retval;
}

static int mtp_uds_connect_or_listen(UNUSED const char* function_name,
                                     amxc_var_t* args,
                                     UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* mtp_path = GET_CHAR(args, "MTP");
    char* alias = NULL;
    amxd_object_t* mtp_object = amxd_dm_findf(mtp_uds_get_dm(), "%s", mtp_path);
    amxd_object_t* uds_obj = amxd_object_get(mtp_object, "UDS");

    alias = amxd_object_get_value(cstring_t, mtp_object, "Alias", NULL);
    when_str_empty(alias, exit);

    if(strcmp(alias, "mtp-uds-host") == 0) {
        retval = mtp_uds_connect(uds_obj);
        when_failed(retval, exit);
    } else if(strcmp(alias, "mtp-uds-container") == 0) {
        retval = mtp_uds_listen(uds_obj);
        when_failed(retval, exit);
    } else {
        goto exit;
    }

    retval = 0;
exit:
    free(alias);
    return retval;
}

static int mtp_uds_init(UNUSED const char* function_name,
                        amxc_var_t* args,
                        UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* usp_path = GET_CHAR(args, "path");
    amxo_parser_t* parser = mtp_uds_get_parser();
    amxd_object_t* object = usp_get_object();
    amxc_var_t eid;
    SAH_TRACEZ_INFO(ME, "Init called for %s", usp_path);

    amxc_var_init(&eid);

    when_null(parser, exit);

    amxd_object_get_param(object, "EndpointID", &eid);

    retval = amxo_parser_set_config(parser, "usp.EndpointID", &eid);

exit:
    amxc_var_clean(&eid);
    return retval;
}

static int mtp_uds_send_response(UNUSED const char* function_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    int retval = -1;
    const char* mtp_path = GET_CHAR(args, "MTP");
    const char* to_id = GET_CHAR(args, "to_id");
    uint32_t type = amxc_var_dyncast(uint32_t, GET_ARG(args, "type"));
    uint32_t sender_con_type = 0;
    uint32_t receiver_con_type = 0;
    uspl_tx_t* usp_tx = NULL;
    uspi_con_t* con = NULL;

    SAH_TRACEZ_INFO(ME, "Send response using mtp [%s] to [%s]", mtp_path, to_id);

    retval = mtp_utils_build_response(type, args, &usp_tx);
    when_failed(retval, exit);

    // This gives the type of the sender, but we need the con type that will receive this message
    sender_con_type = uspi_utils_get_con_type(type);
    receiver_con_type = (sender_con_type == USPI_CON_AGENT) ? USPI_CON_CONTROLLER : USPI_CON_AGENT;

    con = mtp_uds_con_from_eid(to_id, receiver_con_type);
    when_null(con, exit);

    mtp_uds_send_message(con, usp_tx);

    retval = 0;
exit:
    uspl_tx_delete(&usp_tx);
    return retval;
}

static uspi_con_t* mtp_uds_con_from_eid_listen(usp_connections_t* conns,
                                               uint32_t con_type,
                                               const char* eid) {
    uspi_con_t* con = NULL;
    uspi_con_t* result = NULL;

    // If we are looking for an agent socket, start from controller listen socket
    if((con_type & USPI_CON_AGENT) == USPI_CON_AGENT) {
        con = conns->con_contr;
    } else {
        con = conns->con_agent;
    }

    amxc_llist_for_each(lit, &con->accepted_cons) {
        con = amxc_container_of(lit, uspi_con_t, lit);
        if((con->eid != NULL) && (strcmp(con->eid, eid) == 0)) {
            result = con;
            goto exit;
        }
    }

exit:
    return result;
}

static uspi_con_t* mtp_uds_con_from_eid_connect(usp_connections_t* conns,
                                                uint32_t con_type,
                                                const char* eid) {
    uspi_con_t* con = NULL;
    uspi_con_t* result = NULL;

    if((con_type & USPI_CON_AGENT) == USPI_CON_AGENT) {
        con = conns->con_agent;
    } else {
        con = conns->con_contr;
    }

    if((con->eid != NULL) && (strcmp(con->eid, eid) == 0)) {
        result = con;
    }

    return result;
}

static CONSTRUCTOR int mtp_uds_module_start(void) {
    amxm_module_t* mod_usp = NULL;

    amxm_module_register(&mod_usp, NULL, "UDS");
    amxm_module_add_function(mod_usp, "init", mtp_uds_init);
    amxm_module_add_function(mod_usp, "connect", mtp_uds_connect_or_listen);
    amxm_module_add_function(mod_usp, "disconnect", mtp_uds_disconnect);
    amxm_module_add_function(mod_usp, "send_response", mtp_uds_send_response);

    return 0;
}

static DESTRUCTOR int mtp_uds_module_stop(void) {
    amxm_module_t* mod_usp = amxm_get_module(NULL, "usp");
    amxm_module_deregister(&mod_usp);
    return 0;
}

int mtp_uds_send_message(uspi_con_t* con, uspl_tx_t* usp_tx) {
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* tlv_pbuf = NULL;
    int retval = -1;

    imtp_frame_new(&frame);
    imtp_tlv_new(&tlv_pbuf, imtp_tlv_type_protobuf_bytes, usp_tx->pbuf_len,
                 usp_tx->pbuf, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(frame, tlv_pbuf);

    SAH_TRACEZ_INFO(ME, "Sending frame on UDS connection with fd: [%d]", uspi_con_get_fd(con));
    retval = uspi_con_write_frame(con, frame);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending message on UDS connection");
        goto exit;
    }

    retval = 0;
exit:
    imtp_frame_delete(&frame);
    return retval;
}

uspi_con_t* mtp_uds_con_from_eid(const char* eid, uint32_t con_type) {
    amxd_object_t* mtp = amxd_dm_findf(usp_get_dm(), "LocalAgent.MTP.");
    uspi_con_t* result = NULL;

    amxd_object_for_each(instance, it, mtp) {
        amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* uds = NULL;
        char* protocol = amxd_object_get_value(cstring_t, inst, "Protocol", NULL);
        uspi_con_t* con = NULL;
        usp_connections_t* conns = NULL;

        if((protocol == NULL) || (strcmp(protocol, "UDS") != 0)) {
            free(protocol);
            continue;
        }
        free(protocol);

        uds = amxd_object_findf(inst, "UDS.");
        if(uds == NULL) {
            continue;
        }

        conns = (usp_connections_t*) uds->priv;
        if(conns == NULL) {
            continue;
        }

        // Does this MTP setup listen sockets or connect to listen sockets?
        if((conns->con_agent->flags & USPI_CON_LISTEN) == USPI_CON_LISTEN) {
            con = mtp_uds_con_from_eid_listen(conns, con_type, eid);
        } else {
            con = mtp_uds_con_from_eid_connect(conns, con_type, eid);
        }
        if(con != NULL) {
            result = con;
            goto exit;
        }
    }

exit:
    return result;
}
