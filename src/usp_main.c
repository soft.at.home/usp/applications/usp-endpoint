/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <time.h>

#include <debug/sahtrace.h>

#include "usp_main.h"
#include "dm_usp.h"
#include "dm_usp_mtp.h"

#include "agent/agent.h"
#include "controller/controller.h"

#define ME "usp"

typedef struct _module {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxb_bus_ctx_t* bus_ctx;
    amxd_object_t* object;
    struct timespec start_tv;
    const char* type;           // Agent or Controller
} mod_uspc_t;

static mod_uspc_t module;

static int usp_initialize(amxd_dm_t* dm,
                          amxo_parser_t* parser,
                          const char* type) {
    struct timespec ts;
    int retval = -1;
    amxc_string_t path;
    SAH_TRACEZ_INFO(ME, "USP %s module started", type);

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "Local%s.", type);
    amxp_sigmngr_add_signal(NULL, "connection-added");
    amxp_sigmngr_add_signal(NULL, "connection-deleted");

    /* initialise tv for Uptime calculation */
    clock_gettime(CLOCK_MONOTONIC, &ts);
    module.start_tv.tv_sec = ts.tv_sec;
    module.start_tv.tv_nsec = ts.tv_nsec;

    module.dm = dm;
    module.parser = parser;
    module.bus_ctx = amxb_be_who_has(amxc_string_get(&path, 0));
    module.object = amxd_dm_findf(dm, "Local%s.", type);
    module.type = type;

    dm_usp_init(type);

    retval = 0;

    amxc_string_clean(&path);
    return retval;
}

static void usp_cleanup(amxd_dm_t* dm,
                        UNUSED amxo_parser_t* parser,
                        const char* type) {
    SAH_TRACEZ_INFO(ME, "USP %s module stopped", type);
    const char* file = GET_CHAR(&parser->config, "save_file");
    amxd_object_t* lc = amxd_dm_findf(dm, "Local%s.", type);
    amxd_object_t* mtps = amxd_object_findf(lc, "MTP.");

    if((file != NULL) && (*file != 0)) {
        amxo_parser_save_object(parser, file, lc, false);
    }

    amxd_object_for_all(mtps, "[Status == 'Up'].", usp_mtp_disable, NULL);

    module.dm = NULL;
    module.parser = NULL;
    module.bus_ctx = NULL;
}

amxd_dm_t* usp_get_dm(void) {
    return module.dm;
}

amxb_bus_ctx_t* usp_get_bus_ctx(void) {
    return module.bus_ctx;
}

amxo_parser_t* usp_get_parser(void) {
    return module.parser;
}

amxd_object_t* usp_get_object(void) {
    return module.object;
}

uint32_t usp_get_uptime(void) {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec - module.start_tv.tv_sec;
}

const char* usp_get_type(void) {
    return module.type;
}

int _controller_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case 0:     // START
        retval = usp_initialize(dm, parser, "Controller");
        controller_init();
        break;
    case 1:     // STOP
        usp_cleanup(dm, parser, "Controller");
        break;
    default:
        break;
    }

    return retval;
}

int _agent_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case 0:     // START
        retval = usp_initialize(dm, parser, "Agent");
        agent_init();
        break;
    case 1:     // STOP
        usp_cleanup(dm, parser, "Agent");
        break;
    default:
        break;
    }

    return retval;
}

int _endpoint_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case 0:     // START
        retval = usp_initialize(dm, parser, "Agent");
        controller_init();
        agent_init();
        break;
    case 1:     // STOP
        usp_cleanup(dm, parser, "Agent");
        break;
    default:
        break;
    }

    return retval;
}
