/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <uspi/uspi_connection.h>
#include <uspi/uspi_subscription.h>
#include <uspi/uspi_utils.h>

#include "usp_main.h"
#include "mtp_utils.h"
#include "mtp/mtp_mqtt.h"
#include "mtp/mtp_uds.h"
#include "agent/dm_subscription.h"

#define ME "agent"
#define USPA_SUB_RETRY_MAX 9

static int notification_send(uspl_tx_t* usp_tx, amxc_var_t* mtp_info) {
    const char* protocol = GET_CHAR(mtp_info, "protocol");
    uspi_con_t* con = NULL;
    int retval = -1;

    when_str_empty(protocol, exit);

    if(strcmp(protocol, "MQTT") == 0) {
        SAH_TRACEZ_INFO(ME, "Send USP Notify over MQTT");
        amxd_object_t* la_mtp = amxd_dm_findf(usp_get_dm(), "%s", GET_CHAR(mtp_info, "la_mtp"));
        const char* topic = GET_CHAR(mtp_info, "topic");
        when_null(la_mtp, exit);
        when_str_empty(topic, exit);

        con = (uspi_con_t*) la_mtp->priv;
        when_null(con, exit);
        retval = mtp_mqtt_imtp_send(con, usp_tx, NULL, NULL, topic, la_mtp);
    } else if(strcmp(protocol, "UDS") == 0) {
        SAH_TRACEZ_INFO(ME, "Send USP Notify over UDS");
        con = mtp_uds_con_from_eid(usp_tx->rendpoint_id, USPI_CON_CONTROLLER);
        retval = mtp_uds_send_message(con, usp_tx);
    }

exit:
    return retval;
}

static void notification_retry(UNUSED amxp_timer_t* timer, void* priv) {
    uspi_sub_retry_t* sub_retry = (uspi_sub_retry_t*) priv;
    int retval = -1;
    uspi_sub_t* sub = NULL;
    amxc_var_t mtp_info;
    amxc_string_t* contr_path = NULL;

    SAH_TRACEZ_INFO(ME, "Retrying notification");
    amxc_var_init(&mtp_info);

    sub = amxc_container_of(sub_retry->lit.llist, uspi_sub_t, retry_list);
    when_null(sub, exit);
    contr_path = uspi_utils_add_dot(sub->contr_path);

    retval = amxb_call(usp_get_bus_ctx(), amxc_string_get(contr_path, 0), "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    if(sub_retry->retry_attempts < USPA_SUB_RETRY_MAX) {
        uint32_t interval_mult = 0;
        amxc_var_t ret;
        amxc_var_init(&ret);
        amxb_get(usp_get_bus_ctx(), amxc_string_get(contr_path, 0), 0, &ret, 5);
        interval_mult = GETP_UINT32(&ret, "0.0.USPNotifRetryIntervalMultiplier");
        (sub_retry->retry_attempts)++;
        (sub_retry->retry_time) *= (interval_mult / 1000);
        amxc_var_clean(&ret);
    }

    notification_send(sub_retry->usp_tx, GETI_ARG(&mtp_info, 0));

    SAH_TRACEZ_NOTICE(ME, "Next USP notification retry with id %s in %d seconds",
                      sub->id, sub_retry->retry_time);
    amxp_timer_start(sub_retry->retry_timer, 1000 * sub_retry->retry_time);

exit:
    amxc_string_delete(&contr_path);
    amxc_var_clean(&mtp_info);
}

static void oper_complete_notify(UNUSED const char* const sig_name,
                                 UNUSED const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_WARNING(ME, "Asynchronous operations not yet implemented in this component");
}

static int subscribe_to_path(amxd_path_t* path,
                             const char* notif_type,
                             uspi_sub_ref_t* sub_ref) {
    int retval = -1;
    amxb_bus_ctx_t* ctx = usp_get_bus_ctx();
    char* expr = NULL;

    expr = uspi_subscription_build_expression(path, notif_type);
    SAH_TRACEZ_INFO(ME, "Creating subscription for %s with expression %s",
                    amxd_path_get(path, AMXD_OBJECT_TERMINATE), expr);

    retval = amxb_subscribe(ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE), expr,
                            dm_subscription_notify, sub_ref);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to create subscription");
    }

    free(expr);
    return retval;
}

static int unsubscribe_from_path(amxd_path_t* path, uspi_sub_ref_t* sub_ref) {
    amxb_bus_ctx_t* ctx = usp_get_bus_ctx();
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Removing subscription for %s", amxd_path_get(path, AMXD_OBJECT_TERMINATE));
    retval = amxb_unsubscribe(ctx, amxd_path_get(path, AMXD_OBJECT_TERMINATE),
                              dm_subscription_notify, sub_ref);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to unsubscribe to %s",
                           amxd_path_get(path, AMXD_OBJECT_TERMINATE));
    }

    return retval;
}

static void subscribe_oper_complete(amxd_path_t* path, uspi_sub_t* sub) {
    amxc_string_t expr;
    const char* command = NULL;

    amxc_string_init(&expr, 0);
    amxc_string_setf(&expr, "path starts with '%s'", amxd_path_get(path, AMXD_OBJECT_TERMINATE));

    command = amxd_path_get_param(path);
    if(command != NULL) {
        amxc_string_appendf(&expr, " && cmd_name == '%s'", command);
    }
    SAH_TRACEZ_INFO(ME, "Subscribing to %s signals with expression: %s",
                    USPA_OPER_COMPL, amxc_string_get(&expr, 0));
    amxp_slot_connect(NULL, USPA_OPER_COMPL, amxc_string_get(&expr, 0),
                      oper_complete_notify, sub);

    amxc_string_clean(&expr);
    return;
}

static void subscription_handle_ref(const char* ref_path,
                                    const char* notif_type,
                                    uspi_sub_t* sub,
                                    amxc_var_t* forwarding_list) {
    amxd_path_t path;
    uspi_sub_ref_t* sub_ref = uspi_sub_ref_new(sub, ref_path);

    amxd_path_init(&path, ref_path);
    when_null(sub_ref, exit);

    if(!mtp_utils_is_local(ref_path)) {
        amxc_var_add(cstring_t, forwarding_list, ref_path);
        goto exit;
    }
    if(strcmp("OperationComplete", notif_type) == 0) {
        subscribe_oper_complete(&path, sub);
    } else {
        subscribe_to_path(&path, notif_type, sub_ref);
    }

exit:
    amxd_path_clean(&path);
}

static void dm_subscription_filter_params(amxc_var_t* params) {
    amxc_var_t* ref_list = GET_ARG(params, "ReferenceList");
    amxc_var_t* alias = GET_ARG(params, "Alias");
    amxc_var_t* recipient = GET_ARG(params, "Recipient");
    amxc_var_t* creation_date = GET_ARG(params, "CreationDate");
    amxc_var_t* last_value = GET_ARG(params, "LastValue");
    amxc_var_delete(&ref_list);
    amxc_var_delete(&alias);
    amxc_var_delete(&recipient);
    amxc_var_delete(&creation_date);
    amxc_var_delete(&last_value);
}

// Note: When forwarding subscriptions to the host, it is possible that a subscription instance with
// the same ID already exists. This could be a problem.
static void subscription_forward(amxc_var_t* forwarding_list, amxc_var_t* params) {
    amxb_bus_ctx_t* bus_ctx = mtp_uds_get_host_ctx(USPI_CON_CONTROLLER);
    int retval = 0;
    amxc_var_t* ref_list = NULL;
    amxc_var_t ret;

    amxc_var_init(&ret);

    dm_subscription_filter_params(params);
    ref_list = amxc_var_add_key(amxc_llist_t, params, "ReferenceList", NULL);

    amxc_var_for_each(ref, forwarding_list) {
        amxc_var_add(cstring_t, ref_list, amxc_var_constcast(cstring_t, ref));
    }
    amxc_var_cast(ref_list, AMXC_VAR_ID_CSTRING);

    retval = amxb_add(bus_ctx, "LocalAgent.Subscription.", 0, NULL, params, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to forward subscription to host");
        goto exit;
    }

exit:
    amxc_var_clean(&ret);
}

static int subscription_forwarded_remove(const char* id) {
    amxb_bus_ctx_t* ctx = mtp_uds_get_host_ctx(USPI_CON_CONTROLLER);
    amxc_string_t sub_path;
    amxc_var_t ret;
    int retval = -1;

    amxc_string_init(&sub_path, 0);
    amxc_var_init(&ret);

    amxc_string_setf(&sub_path, "LocalAgent.Subscription.[ID == '%s'].", id);
    retval = amxb_del(ctx, amxc_string_get(&sub_path, 0), 0, NULL, &ret, 5);

    amxc_string_clean(&sub_path);
    amxc_var_clean(&ret);
    return retval;
}

static void subscription_remove_ref(const char* ref,
                                    const char* notif_type,
                                    uspi_sub_t* sub,
                                    amxc_var_t* forwarding_list) {
    amxd_path_t path;

    amxd_path_init(&path, ref);

    if(!mtp_utils_is_local(ref)) {
        amxc_var_add(cstring_t, forwarding_list, ref);
        goto exit;
    }

    if(strcmp("OperationComplete", notif_type) == 0) {
        amxp_slot_disconnect_with_priv(NULL, oper_complete_notify, sub);
    } else {
        uspi_sub_ref_t* sub_ref = uspi_sub_ref_find(sub, ref);
        unsubscribe_from_path(&path, sub_ref);
    }

exit:
    amxd_path_clean(&path);
}

// TODO: Add checker functions for mandatory parameters
void _Subscription_Added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    int retval = -1;
    uspi_sub_t* sub = NULL;
    amxc_string_t inst_path;
    amxd_object_t* object = amxd_dm_signal_get_object(usp_get_dm(), data);
    uint32_t index = GET_UINT32(data, "index");
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_var_t* recipient = GET_ARG(params, "Recipient");
    const char* id = GET_CHAR(params, "ID");
    const char* notif_type = GET_CHAR(params, "NotifType");
    amxc_var_t* refs = GET_ARG(params, "ReferenceList");
    const amxc_llist_t* ref_list = NULL;
    amxc_var_t forwarding_list;
    SAH_TRACEZ_INFO(ME, "Subscription instance added");

    amxc_var_init(&forwarding_list);
    amxc_var_set_type(&forwarding_list, AMXC_VAR_ID_LIST);
    amxc_string_init(&inst_path, 0);
    amxc_string_setf(&inst_path, "LocalAgent.Subscription.%d.", index);

    retval = uspi_sub_new(&sub, amxc_string_get(&inst_path, 0),
                          amxc_var_constcast(cstring_t, recipient), id, notif_type);
    when_failed(retval, exit);
    object = amxd_object_get_instance(object, NULL, GET_UINT32(data, "index"));
    object->priv = sub;

    amxc_var_cast(refs, AMXC_VAR_ID_LIST);
    ref_list = amxc_var_constcast(amxc_llist_t, refs);

    amxc_llist_for_each(it, ref_list) {
        const char* ref_path = amxc_var_constcast(cstring_t, amxc_var_from_llist_it(it));
        subscription_handle_ref(ref_path, notif_type, sub, &forwarding_list);
    }

    if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &forwarding_list))) {
        subscription_forward(&forwarding_list, params);
    }

exit:
    amxc_var_clean(&forwarding_list);
    amxc_string_clean(&inst_path);
    return;
}

void _Subscription_Changed(UNUSED const char* const sig_name,
                           UNUSED const amxc_var_t* const data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Subscription instance changed");
}

amxd_status_t _subscription_clean(amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  UNUSED amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  UNUSED amxc_var_t* const retval,
                                  UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "_subscription_clean");
    uspi_sub_t* sub = (uspi_sub_t*) object->priv;
    amxc_var_t* ref_list = NULL;
    const char* notif_type = NULL;
    amxc_var_t params;
    amxc_var_t forwarding_list;

    amxc_var_init(&params);
    amxc_var_init(&forwarding_list);
    amxc_var_set_type(&forwarding_list, AMXC_VAR_ID_LIST);

    when_null(sub, exit);

    amxd_object_get_params(object, &params, amxd_dm_access_private);
    ref_list = GET_ARG(&params, "ReferenceList");
    amxc_var_cast(ref_list, AMXC_VAR_ID_LIST);
    notif_type = GET_CHAR(&params, "NotifType");

    amxc_var_for_each(ref, ref_list) {
        subscription_remove_ref(amxc_var_constcast(cstring_t, ref), notif_type,
                                sub, &forwarding_list);
    }

    if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &forwarding_list))) {
        subscription_forwarded_remove(sub->id);
    }

exit:
    amxc_var_clean(&forwarding_list);
    amxc_var_clean(&params);
    uspi_sub_delete(&sub);

    return amxd_status_ok;
}

static uspl_tx_t* dm_subscription_build_tx(uspi_sub_t* sub) {
    amxd_dm_t* dm = usp_get_dm();
    amxd_object_t* contr_obj = NULL;
    amxd_object_t* agent_obj = NULL;
    char* from_id = NULL;
    char* to_id = NULL;
    uspl_tx_t* usp_tx = NULL;

    when_null(sub, exit);

    contr_obj = amxd_dm_findf(dm, "%s", sub->contr_path);
    agent_obj = amxd_dm_findf(dm, "%s", "LocalAgent.");

    from_id = amxd_object_get_value(cstring_t, agent_obj, "EndpointID", NULL);
    to_id = amxd_object_get_value(cstring_t, contr_obj, "EndpointID", NULL);
    uspl_tx_new(&usp_tx, from_id, to_id);

exit:
    free(from_id);
    free(to_id);
    return usp_tx;
}

void dm_subscription_notify(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            void* const priv) {
    SAH_TRACEZ_INFO(ME, "Notify controller");
    int retval = 1;
    uspi_sub_ref_t* sub_ref = (uspi_sub_ref_t*) priv;
    uspi_sub_t* sub = sub_ref->sub;
    amxc_var_t mtp_info;
    amxc_var_t data_copy;
    uspl_tx_t* usp_tx = NULL;
    bool retry = false;
    amxb_bus_ctx_t* bus_ctx = usp_get_bus_ctx();
    amxd_object_t* subs_obj = amxd_dm_findf(usp_get_dm(), "%s", sub->instance_path);
    amxc_string_t* contr_path = NULL;

    amxc_var_init(&mtp_info);
    amxc_var_init(&data_copy);
    // uspi_subscription_build_notify will modify variant, so we need a copy
    amxc_var_copy(&data_copy, data);

    contr_path = uspi_utils_add_dot(sub->contr_path);
    retval = amxb_call(bus_ctx, amxc_string_get(contr_path, 0), "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    usp_tx = dm_subscription_build_tx(sub);
    when_null(usp_tx, exit);

    // TODO check NotifExpiration > 0
    retry = amxd_object_get_value(bool, subs_obj, "NotifRetry", NULL);
    do {
        retval = uspi_subscription_build_notify(&data_copy, sub_ref, usp_tx, retry);
        if(retval < 0) {
            uspl_tx_delete(&usp_tx);
            goto exit;
        } else {
            notification_send(usp_tx, GETI_ARG(&mtp_info, 0));

            if(retry) {
                uspi_sub_retry_new(sub, usp_tx, notification_retry, bus_ctx);
            } else {
                uspl_tx_delete(&usp_tx);
            }
        }
    } while(retval == 1);

exit:
    amxc_string_delete(&contr_path);
    amxc_var_clean(&data_copy);
    amxc_var_clean(&mtp_info);
    return;
}

int dm_subscription_notify_controller(uspi_sub_t* sub, amxc_var_t* notification) {
    SAH_TRACEZ_INFO(ME, "Notify controller");
    amxb_bus_ctx_t* bus_ctx = usp_get_bus_ctx();
    int retval = -1;
    amxc_var_t mtp_info;
    uspl_tx_t* usp_tx = NULL;
    bool retry = false;
    amxc_string_t* contr_path = NULL;
    amxd_object_t* subs_obj = NULL;

    amxc_var_init(&mtp_info);

    when_null(sub, exit);

    contr_path = uspi_utils_add_dot(sub->contr_path);
    retval = amxb_call(bus_ctx, amxc_string_get(contr_path, 0), "GetMTPInfo", NULL, &mtp_info, 5);
    when_failed(retval, exit);

    subs_obj = amxd_dm_findf(usp_get_dm(), "%s", sub->instance_path);
    usp_tx = dm_subscription_build_tx(sub);
    when_null(usp_tx, exit);

    retval = uspl_notify_new(usp_tx, notification);
    if(retval < 0) {
        goto exit;
    } else {
        notification_send(usp_tx, GETI_ARG(&mtp_info, 0));

        retry = amxd_object_get_value(bool, subs_obj, "NotifRetry", NULL);
        if(retry) {
            uspi_sub_retry_new(sub, usp_tx, notification_retry, bus_ctx);
        } else {
            uspl_tx_delete(&usp_tx);
        }
    }

exit:
    amxc_string_delete(&contr_path);
    amxc_var_clean(&mtp_info);
    return retval;
}

uspi_sub_t* dm_subscription_get_sub(const char* sub_id) {
    uspi_sub_t* sub = NULL;
    amxd_dm_t* dm = usp_get_dm();
    amxd_object_t* object = amxd_dm_findf(dm, "LocalAgent.Subscription.[ID=='%s'].", sub_id);

    when_null(object, exit);
    sub = (uspi_sub_t*) object->priv;

exit:
    return sub;
}
