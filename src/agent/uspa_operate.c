/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>

#include <usp/uspl.h>

#include "usp_main.h"
#include "mtp_utils.h"
#include "agent/agent.h"

#define ME "agent"

/*
   Take output args in return variant at index 1 if they exist.
   Optionally add return variant with argument name '_retval' to output args.
 */
static void uspa_operate_build_resp_success(const char* command,
                                            amxc_llist_t* resp_list,
                                            amxc_var_t* ret) {
    amxc_var_t* response = NULL;
    amxc_var_t* output_args = NULL;
    amxc_var_t* ret_var = GETI_ARG(ret, 0);
    amxc_var_t* out_args_var = GETI_ARG(ret, 1);

    amxc_var_new(&response);

    amxc_var_set_type(response, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, response, "executed_command", command);
    amxc_var_add_key(uint32_t, response, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_REQ_OUTPUT_ARGS);
    output_args = amxc_var_add_key(amxc_htable_t, response, "output_args", NULL);

    if(out_args_var != NULL) {
        amxc_var_move(output_args, out_args_var);
    }

    if(ret_var != NULL) {
        amxc_var_t* _retval = amxc_var_add_key(amxc_htable_t, output_args, "_retval", NULL);
        amxc_var_move(_retval, ret_var);
    }

    amxc_llist_append(resp_list, &response->lit);
}

static void uspa_operate_build_resp_failure(const char* command,
                                            amxc_llist_t* resp_list,
                                            amxc_var_t* ret) {
    amxc_var_t* response = NULL;
    amxc_var_t* result = amxc_var_take_index(ret, 1);

    amxc_var_new(&response);

    amxc_var_set_type(response, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, response, "executed_command", command);
    amxc_var_add_key(uint32_t, response, "operation_resp_case", USP__OPERATE_RESP__OPERATION_RESULT__OPERATION_RESP_CMD_FAILURE);
    if(result != NULL) {
        amxc_var_set_key(response, "cmd_failure", result, AMXC_VAR_FLAG_DEFAULT);
    } else {
        amxc_var_t* cmd_failure = amxc_var_add_key(amxc_htable_t, response, "cmd_failure", NULL);
        amxc_var_add_key(uint32_t, cmd_failure, "err_code", USP_ERR_COMMAND_FAILURE);
        amxc_var_add_key(cstring_t, cmd_failure, "err_msg", uspl_error_code_to_str(USP_ERR_COMMAND_FAILURE));
    }
    amxc_llist_append(resp_list, &response->lit);
}

static int uspa_invoke_operate(amxc_var_t* request, amxc_llist_t* resp_list, bool resp_needed) {
    int retval = -1;
    const char* command = GET_CHAR(request, "command");
    const char* object_path = NULL;
    const char* function_path = NULL;
    amxb_bus_ctx_t* ctx = mtp_utils_get_ctx(command);
    amxd_path_t cmd_path;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxd_path_init(&cmd_path, command);

    object_path = amxd_path_get(&cmd_path, AMXD_OBJECT_TERMINATE);
    function_path = amxd_path_get_param(&cmd_path);
    when_str_empty(function_path, exit);

    amxb_set_access(ctx, AMXB_PUBLIC);
    retval = amxb_call(ctx, object_path, function_path, GET_ARG(request, "input_args"), &ret, 5);
    amxb_set_access(ctx, AMXB_PROTECTED);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to invoke %s%s", object_path, function_path);
    }

    if(resp_needed) {
        if(retval == 0) {
            uspa_operate_build_resp_success(command, resp_list, &ret);
        } else {
            uspa_operate_build_resp_failure(command, resp_list, &ret);
        }
    }

exit:
    amxc_var_clean(&ret);
    amxd_path_clean(&cmd_path);
    return 0;
}

void uspa_handle_operate(UNUSED const char* const sig_name,
                         const amxc_var_t* const msg,
                         UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Operate");
    amxc_var_t* request = GET_ARG(msg, "data");
    amxc_llist_t resp_list;
    amxc_var_t response;
    amxc_var_t* data = NULL;
    bool resp_needed = GET_BOOL(request, "send_resp");

    amxc_llist_init(&resp_list);
    amxc_var_init(&response);

    uspa_invoke_operate(request, &resp_list, resp_needed);
    when_false(resp_needed, exit);

    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);
    amxc_var_set_key(&response, "to_id", GET_ARG(msg, "from_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&response, "from_id", GET_ARG(msg, "to_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&response, "msg_id", GET_ARG(msg, "msg_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_add_key(uint32_t, &response, "type", USP__HEADER__MSG_TYPE__OPERATE_RESP);
    amxc_var_set_key(&response, "MTP", GET_ARG(msg, "MTP"), AMXC_VAR_FLAG_DEFAULT);
    data = amxc_var_add_key(amxc_llist_t, &response, "data", NULL);
    amxc_llist_for_each(it, (&resp_list)) {
        amxc_var_t* d = amxc_var_from_llist_it(it);
        amxc_var_set_index(data, -1, d, AMXC_VAR_FLAG_DEFAULT);
    }
    agent_send_response(GET_CHAR(&response, "MTP"), &response);

exit:
    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_var_clean(&response);

    return;
}