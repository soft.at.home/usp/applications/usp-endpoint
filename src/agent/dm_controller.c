/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <debug/sahtrace.h>

#include "usp_main.h"
#include "agent/dm_controller.h"

#define ME "agent"

static void add_matching_la_mtp_mqtt(amxd_object_t* mqtt, amxc_var_t* ret) {
    char* reference = amxd_object_get_value(cstring_t, mqtt, "Reference", NULL);
    amxd_object_t* la_mtp_mqtt = amxd_dm_findf(usp_get_dm(),
                                               "LocalAgent.MTP.[MQTT.Reference == '%s'].MQTT.",
                                               reference);
    if(la_mtp_mqtt != NULL) {
        char* path = amxd_object_get_path(la_mtp_mqtt, AMXD_OBJECT_NAMED);
        amxc_var_add_key(cstring_t, ret, "la_mtp", path);
        free(path);
    }
    free(reference);
}

static void add_matching_la_mtp_uds(amxd_object_t* imtp, amxc_var_t* ret) {
    char* reference = amxd_object_get_value(cstring_t, imtp, "Reference", NULL);
    amxd_object_t* la_mtp_imtp = amxd_dm_findf(usp_get_dm(), "%s.IMTP.", reference);
    if(la_mtp_imtp != NULL) {
        char* path = amxd_object_get_path(la_mtp_imtp, AMXD_OBJECT_NAMED);
        amxc_var_add_key(cstring_t, ret, "la_mtp", path);
        free(path);
    }
    free(reference);
}

amxd_status_t _Controller_GetMTPInfo(amxd_object_t* object,
                                     UNUSED amxd_function_t* func,
                                     UNUSED amxc_var_t* args,
                                     amxc_var_t* ret) {
    amxd_object_t* mtp = amxd_object_findf(object, "MTP");
    amxd_object_t* mtp_instance = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    char* protocol = NULL;

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);

    amxd_object_for_each(instance, it, mtp) {
        bool enabled = false;

        mtp_instance = amxc_container_of(it, amxd_object_t, it);
        enabled = amxd_object_get_value(bool, mtp_instance, "Enable", NULL);
        if(!enabled) {
            continue;
        }

        protocol = amxd_object_get_value(cstring_t, mtp_instance, "Protocol", NULL);
        amxc_var_add_key(cstring_t, ret, "protocol", protocol);
        break;
    }

    if(protocol == NULL) {
        char* path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
        SAH_TRACEZ_WARNING(ME, "Unable to find enabled MTP for controller %s", path);
        free(path);
        goto exit;
    }

    if(strcmp(protocol, "MQTT") == 0) {
        amxd_object_t* mqtt = amxd_object_findf(mtp_instance, "MQTT");
        char* topic = amxd_object_get_value(cstring_t, mqtt, "Topic", NULL);
        amxc_var_add_key(cstring_t, ret, "topic", topic);
        add_matching_la_mtp_mqtt(mqtt, ret);
        free(topic);
    }
    if(strcmp(protocol, "UDS") == 0) {
        amxd_object_t* uds = amxd_object_findf(mtp_instance, "UDS");
        add_matching_la_mtp_uds(uds, ret);
    }

    status = amxd_status_ok;
exit:
    free(protocol);
    return status;
}

char* dm_controller_get_instance_from_eid(const char* eid) {
    amxb_bus_ctx_t* bus_ctx = usp_get_bus_ctx();
    char* result = NULL;
    amxc_var_t* controller = NULL;
    amxc_string_t path;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    when_str_empty(eid, exit);
    amxc_string_setf(&path, "LocalAgent.Controller.[EndpointID=='%s'].", eid);

    amxb_get(bus_ctx, amxc_string_get(&path, 0), 0, &ret, 5);
    controller = GETP_ARG(&ret, "0.0");

    if(controller == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find controller instance with EndpointID = %s", eid);
        goto exit;
    }

    result = strdup(amxc_var_key(controller));

exit:
    amxc_string_clean(&path);
    amxc_var_clean(&ret);
    return result;
}

bool dm_controller_exists(const char* eid) {
    bool retval = false;

    when_str_empty(eid, exit);

    if(amxd_dm_findf(usp_get_dm(), "LocalAgent.Controller.[EndpointID == '%s'].", eid) != NULL) {
        SAH_TRACEZ_INFO(ME, "Controller instance with EID = %s exists", eid);
        retval = true;
    }

exit:
    return retval;
}

int dm_controller_add_inst(const char* eid) {
    int retval = -1;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty(eid, exit);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "EndpointID", eid);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    retval = amxd_trans_apply(&trans, usp_get_dm());
    if(retval == 0) {
        SAH_TRACEZ_INFO(ME, "Added controller instance with EID = %s", eid);
    } else {
        SAH_TRACEZ_WARNING(ME, "Failed to add controller instance with EID = %s", eid);
    }

exit:
    amxd_trans_clean(&trans);
    return retval;
}

int dm_controller_add_mtp_uds(const char* eid) {
    int retval = -1;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty(eid, exit);
    amxd_trans_select_pathf(&trans, "LocalAgent.Controller.[EndpointID == '%s'].MTP.", eid);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDS");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    retval = amxd_trans_apply(&trans, usp_get_dm());
    if(retval == 0) {
        SAH_TRACEZ_INFO(ME, "Added MTP instance for controller with EID = %s", eid);
    } else {
        SAH_TRACEZ_WARNING(ME, "Failed to MTP instance for controller instance with EID = %s", eid);
    }

exit:
    amxd_trans_clean(&trans);
    return retval;
}
