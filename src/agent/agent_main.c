/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "usp_main.h"
#include "agent/agent.h"

#define ME "agent"

void agent_init(void) {
    amxp_sigmngr_add_signal(NULL, "usp:request-1");  // GET
    amxp_sigmngr_add_signal(NULL, "usp:request-4");  // SET
    amxp_sigmngr_add_signal(NULL, "usp:request-6");  // OPERATE
    amxp_sigmngr_add_signal(NULL, "usp:request-8");  // ADD
    amxp_sigmngr_add_signal(NULL, "usp:request-10"); // DEL
    amxp_sigmngr_add_signal(NULL, "usp:request-12"); // GET_SUPPORTED_DM
    amxp_sigmngr_add_signal(NULL, "usp:request-14"); // GET_INSTANCES
    amxp_sigmngr_add_signal(NULL, USPA_OPER_COMPL);  // Operation complete

    amxp_slot_connect(NULL, "usp:request-1", NULL, uspa_handle_get, NULL);
    amxp_slot_connect(NULL, "usp:request-4", NULL, uspa_handle_set, NULL);
    amxp_slot_connect(NULL, "usp:request-6", NULL, uspa_handle_operate, NULL);
    amxp_slot_connect(NULL, "usp:request-8", NULL, uspa_handle_add, NULL);
    amxp_slot_connect(NULL, "usp:request-10", NULL, uspa_handle_del, NULL);
    amxp_slot_connect(NULL, "usp:request-12", NULL, uspa_handle_get_supported_dm, NULL);
    amxp_slot_connect(NULL, "usp:request-14", NULL, uspa_handle_get_instances, NULL);
}

amxd_status_t agent_send_response(const char* mtp, amxc_var_t* response) {
    amxd_status_t status = amxd_status_ok;
    amxd_object_t* mtp_protocol = amxd_dm_findf(usp_get_dm(), "%s", mtp);
    const char* protocol = amxd_object_get_name(mtp_protocol, AMXD_OBJECT_NAMED);
    amxc_var_t ret;

    amxc_var_init(&ret);
    if(!amxm_has_function(NULL, protocol, "send_response")) {
        SAH_TRACEZ_ERROR(ME, "%s.send_response not available", protocol);
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if(amxm_execute_function(NULL, protocol, "send_response", response, &ret) != 0) {
        SAH_TRACEZ_ERROR(ME, "send_response failed");
        status = amxd_status_unknown_error;
    }

exit:
    amxc_var_clean(&ret);
    return status;
}