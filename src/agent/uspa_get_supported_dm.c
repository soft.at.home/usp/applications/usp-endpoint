/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>

#include <usp/uspl.h>

#include "usp_main.h"
#include "agent/agent.h"
#include "mtp_utils.h"

#define ME "agent"

static int uspa_invoke_get_supported(amxc_llist_t* resp_list, const char* path, uint32_t flags) {
    amxb_bus_ctx_t* ctx = mtp_utils_get_ctx(path);
    int retval = -1;
    amxc_var_t* reply = NULL;
    amxc_var_t* resolved = NULL;
    amxc_var_t* result = NULL;
    amxc_var_t rv;

    amxc_var_init(&rv);
    amxc_var_set_type(&rv, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&resolved);

    retval = amxb_get_supported(ctx, path, flags, &rv, 5);
    reply = GETI_ARG(&rv, 0);
    result = amxc_var_add_key(amxc_htable_t, reply, "result", NULL);
    amxc_var_add_key(cstring_t, result, "requested_path", path);

    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "amxb_get_supported failed with status: [%d] for path: [%s]",
                           retval, path);
        amxc_var_add_key(uint32_t, result, "err_code", USP_ERR_INVALID_PATH);
    } else {
        amxc_var_add_key(uint32_t, result, "err_code", 0);
    }

    amxc_var_move(resolved, reply);
    amxc_llist_append(resp_list, &resolved->lit);
    amxc_var_clean(&rv);

    return retval;
}

void uspa_handle_get_supported_dm(UNUSED const char* const sig_name,
                                  const amxc_var_t* const msg,
                                  UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type get_supported_dm");
    amxc_var_t* flag_table = GETP_ARG(msg, "data.flags");
    amxc_var_t* paths = GETP_ARG(msg, "data.paths");
    amxc_llist_t resp_list;
    amxc_var_t response;
    amxc_var_t* data = NULL;
    uint32_t flags = 0;

    amxc_llist_init(&resp_list);
    amxc_var_init(&response);
    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);

    flags |= GET_BOOL(flag_table, "first_level_only") ? AMXB_FLAG_FIRST_LVL : 0;
    flags |= GET_BOOL(flag_table, "return_commands") ? AMXB_FLAG_FUNCTIONS : 0;
    flags |= GET_BOOL(flag_table, "return_events") ? AMXB_FLAG_EVENTS : 0;
    flags |= GET_BOOL(flag_table, "return_params") ? AMXB_FLAG_PARAMETERS : 0;

    amxc_var_for_each(var_path, paths) {
        const char* path = amxc_var_constcast(cstring_t, var_path);
        SAH_TRACEZ_INFO(ME, "Invoke GSDM on path [%s]", path);
        int retval = uspa_invoke_get_supported(&resp_list, path, flags);
        SAH_TRACEZ_INFO(ME, "GSDM done, retval = [%d]", retval);
    }

    amxc_var_set_key(&response, "to_id", GET_ARG(msg, "from_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&response, "from_id", GET_ARG(msg, "to_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&response, "msg_id", GET_ARG(msg, "msg_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_add_key(uint32_t, &response, "type", USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP);
    amxc_var_set_key(&response, "MTP", GET_ARG(msg, "MTP"), AMXC_VAR_FLAG_DEFAULT);
    data = amxc_var_add_key(amxc_llist_t, &response, "data", NULL);
    amxc_llist_for_each(it, (&resp_list)) {
        amxc_var_t* d = amxc_var_from_llist_it(it);
        amxc_var_set_index(data, -1, d, AMXC_VAR_FLAG_DEFAULT);
    }
    agent_send_response(GET_CHAR(&response, "MTP"), &response);

    amxc_llist_clean(&resp_list, variant_list_it_free);
    amxc_var_clean(&response);

    return;
}