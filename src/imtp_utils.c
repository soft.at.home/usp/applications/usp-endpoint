/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <debug/sahtrace.h>

#include <imtp/imtp_connection.h>

#include <usp/uspl.h>

#include "usp.h"
#include "imtp_utils.h"

#define ME "imtp"

typedef int (* usp_request_fn_t) (uspl_rx_t* usp_rx, amxc_var_t* result);
typedef int (* usp_resp_fn_t) (uspl_rx_t* usp_rx, amxc_llist_t* response);

typedef struct _request_cb {
    uint32_t id;
    usp_request_fn_t extract_req_fn;
    usp_resp_fn_t extract_resp_fn;
} usp_request_t;

static void imtp_usp_error_extract(uspl_rx_t* usp_rx) {
    int retval = -1;
    amxc_var_t values;

    amxc_var_init(&values);

    retval = uspl_error_resp_extract(usp_rx, &values);
    if(retval == 0) {
        amxc_var_log(&values);
    }

    amxc_var_clean(&values);
}

int imtp_extract_msg(uspl_rx_t* usp_rx,
                     amxc_var_t* result,
                     amxc_llist_t* response,
                     bool* request) {
    int retval = -1;
    usp_request_t fn[] = {
        { USP__HEADER__MSG_TYPE__ERROR, NULL, NULL },
        { USP__HEADER__MSG_TYPE__GET, uspl_get_extract, NULL },
        { USP__HEADER__MSG_TYPE__GET_RESP, NULL, uspl_get_resp_extract},
        { USP__HEADER__MSG_TYPE__NOTIFY, uspl_notify_extract, NULL},
        { USP__HEADER__MSG_TYPE__SET, uspl_set_extract, NULL },
        { USP__HEADER__MSG_TYPE__SET_RESP, NULL, uspl_set_resp_extract},
        { USP__HEADER__MSG_TYPE__OPERATE, uspl_operate_extract, NULL},
        { USP__HEADER__MSG_TYPE__OPERATE_RESP, NULL, uspl_operate_resp_extract},
        { USP__HEADER__MSG_TYPE__ADD, uspl_add_extract, NULL},
        { USP__HEADER__MSG_TYPE__ADD_RESP, NULL, uspl_add_resp_extract},
        { USP__HEADER__MSG_TYPE__DELETE, uspl_delete_extract, NULL},
        { USP__HEADER__MSG_TYPE__DELETE_RESP, NULL, uspl_delete_resp_extract},
        { USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM, uspl_get_supported_dm_extract, NULL},
        { USP__HEADER__MSG_TYPE__GET_SUPPORTED_DM_RESP, NULL, uspl_get_supported_dm_resp_extract},
        { USP__HEADER__MSG_TYPE__GET_INSTANCES, uspl_get_instances_extract, NULL},
        { USP__HEADER__MSG_TYPE__GET_INSTANCES_RESP, NULL, uspl_get_instances_resp_extract},
        { USP__HEADER__MSG_TYPE__NOTIFY_RESP, NULL, NULL},
        { USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO, NULL, NULL},
        { USP__HEADER__MSG_TYPE__GET_SUPPORTED_PROTO_RESP, NULL, NULL},
        { USP__HEADER__MSG_TYPE__REGISTER, uspl_register_extract, NULL},
        { USP__HEADER__MSG_TYPE__REGISTER_RESP, NULL, uspl_register_resp_extract},
    };

    uint32_t type = uspl_msghandler_msg_type(usp_rx);
    if(type == USP__HEADER__MSG_TYPE__ERROR) {
        SAH_TRACEZ_ERROR(ME, "Received error message from agent");
        imtp_usp_error_extract(usp_rx);
    } else if(type < (sizeof(fn) / sizeof(fn[0]))) {
        if(fn[type].extract_req_fn != NULL) {
            *request = true;
            retval = fn[type].extract_req_fn(usp_rx, result);
        } else if(fn[type].extract_resp_fn != NULL) {
            *request = false;
            retval = fn[type].extract_resp_fn(usp_rx, response);
        } else {
            SAH_TRACEZ_ERROR(ME, "Unknown message format");
        }
    }

    return retval;
}
