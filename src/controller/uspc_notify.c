/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>

#include <usp/uspl.h>
#include <uspi/uspi_subscription.h>

#include "usp_main.h"
#include "controller/controller.h"
#include "agent/dm_subscription.h"

#define ME "controller"

void uspc_handle_notify(UNUSED const char* const sig_name,
                        const amxc_var_t* const msg,
                        UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Notify");
    amxc_var_t* result = GET_ARG(msg, "data");
    bool send_resp = GET_BOOL(result, "send_resp");
    const char* subscription_id = GET_CHAR(result, "subscription_id");
    amxc_var_t response;
    amxc_var_t* data = NULL;
    uspi_sub_t* sub = NULL;

    if(result) {
        amxc_var_log(result);
    }

    // When the plugin is running as USP Endpoint, it may need to forward subscriptions
    sub = dm_subscription_get_sub(subscription_id);
    if(sub != NULL) {
        dm_subscription_notify_controller(sub, result);
    }

    if(!send_resp) {
        goto exit;
    }
    amxc_var_init(&response);
    amxc_var_set_type(&response, AMXC_VAR_ID_HTABLE);

    amxc_var_set_key(&response, "to_id", GET_ARG(msg, "from_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&response, "from_id", GET_ARG(msg, "to_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_set_key(&response, "msg_id", GET_ARG(msg, "msg_id"), AMXC_VAR_FLAG_DEFAULT);
    amxc_var_add_key(uint32_t, &response, "type", USP__HEADER__MSG_TYPE__NOTIFY_RESP);
    amxc_var_set_key(&response, "MTP", GET_ARG(msg, "MTP"), AMXC_VAR_FLAG_DEFAULT);
    data = amxc_var_add_key(amxc_llist_t, &response, "data", NULL);
    amxc_var_add(cstring_t, data, subscription_id);
    controller_send_response(GET_CHAR(&response, "MTP"), &response);

    amxc_var_clean(&response);

exit:
    return;
}