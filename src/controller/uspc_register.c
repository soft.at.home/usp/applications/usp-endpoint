/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_function.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>

#include <usp/uspl.h>
#include <uspi/uspi_connection.h>

#include "usp_main.h"
#include "controller/controller.h"
#include "mtp/mtp_uds.h"

#define ME "controller"

static int uspc_register_forward(amxc_var_t* request) {
    uspi_con_t* con = mtp_uds_get_host_con(USPI_CON_CONTROLLER);
    amxd_object_t* root = usp_get_object();
    uspl_tx_t* usp_tx = NULL;
    amxc_var_t from_id;
    int retval = -1;

    amxc_var_init(&from_id);

    when_null(con, exit);
    when_str_empty(con->eid, exit);

    amxd_object_get_param(root, "EndpointID", &from_id);

    uspl_tx_new(&usp_tx, amxc_var_constcast(cstring_t, &from_id), con->eid);
    uspl_register_new(usp_tx, request);

    retval = mtp_uds_send_message(con, usp_tx);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to send Register message on UDS connection");
    }

exit:
    uspl_tx_delete(&usp_tx);
    amxc_var_clean(&from_id);
    return retval;
}

// For now just forward register messages over the IMTP we are connected with
void uspc_handle_register(UNUSED const char* const sig_name,
                          const amxc_var_t* const msg,
                          UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Handle USP message of type Register");
    amxc_var_t* request = GET_ARG(msg, "data");

    uspc_register_forward(request);
}

