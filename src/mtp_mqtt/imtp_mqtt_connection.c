/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <uspi/uspi_connection.h>

#include "imtp_utils.h"
#include "mtp_utils.h"
#include "mtp/mtp_mqtt.h"
#include "controller/controller.h"

#define ME "mtp_mqtt"

static int imtp_mqtt_unpack_frame(imtp_frame_t* frame,
                                  char** topic,
                                  const imtp_tlv_t** protobuf) {
    int retval = -1;
    const imtp_tlv_t* tlv_next = NULL;

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_topic);
    if(tlv_next != NULL) {
        (*topic) = (char*) calloc(1, tlv_next->length + 1);
        when_null(*topic, exit);
        strncpy((*topic), (char*) tlv_next->value + tlv_next->offset, tlv_next->length);
    } else {
        SAH_TRACEZ_WARNING(ME, "Did not find topic in TLV chain");
    }

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_protobuf_bytes);
    if(tlv_next != NULL) {
        *protobuf = tlv_next;
    } else {
        SAH_TRACEZ_WARNING(ME, "Did not find protobuf in TLV chain");
    }

    tlv_next = imtp_frame_get_first_tlv(frame, imtp_tlv_type_eid);
    if(tlv_next != NULL) {
        SAH_TRACEZ_INFO(ME, "Ignoring TLV of type EID on MQTT IMTP connection");
    }

    retval = 0;
exit:
    return retval;
}

static void imtp_mqtt_handle_request(amxd_object_t* mtp, amxc_var_t* msg, uspl_rx_t* usp_data) {
    mtp_utils_handle_request(mtp, msg, usp_data);
}

static void imtp_mqtt_handle_response(amxc_llist_t* response, uspl_rx_t* usp_data) {
    amxc_var_t data;
    amxc_var_t* vr = NULL;
    amxc_var_t* header = NULL;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    header = amxc_var_add_key(amxc_htable_t, &data, "header", NULL);

    amxc_var_add_key(cstring_t, header, "msg_id", uspl_msghandler_msg_id(usp_data));
    amxc_var_add_key(cstring_t, header, "from_id", uspl_msghandler_from_id(usp_data));
    amxc_var_add_key(cstring_t, header, "to_id", uspl_msghandler_to_id(usp_data));
    amxc_var_add_key(uint32_t, header, "type", uspl_msghandler_msg_type(usp_data));
    vr = amxc_var_add_key(amxc_llist_t, &data, "response", NULL);
    amxc_llist_for_each(it, response) {
        amxc_var_t* d = amxc_var_from_llist_it(it);
        amxc_var_log(d);
        amxc_var_set_index(vr, -1, d, AMXC_VAR_FLAG_DEFAULT);
    }
    amxp_sigmngr_trigger_signal(NULL, "usp:response", &data);
    amxc_var_clean(&data);
}

static void imtp_mqtt_handle_msg(const imtp_tlv_t* tlv_protobuf, amxd_object_t* mtp) {
    unsigned char* pbuf = (unsigned char*) tlv_protobuf->value + tlv_protobuf->offset;
    int pbuf_len = tlv_protobuf->length;
    uspl_rx_t* usp_data = uspl_msghandler_unpack_protobuf(pbuf, pbuf_len);
    amxc_var_t msg;
    amxc_var_t* result = NULL;
    amxc_llist_t response;
    bool is_request = false;
    int retval = -1;

    amxc_var_init(&msg);
    amxc_llist_init(&response);

    when_null(usp_data, exit);
    SAH_TRACEZ_INFO(ME, "USP RX type = %d", uspl_msghandler_msg_type(usp_data));

    amxc_var_set_type(&msg, AMXC_VAR_ID_HTABLE);
    result = amxc_var_add_new_key(&msg, "data");
    retval = imtp_extract_msg(usp_data, result, &response, &is_request);
    when_failed(retval, exit);

    if(is_request) {
        imtp_mqtt_handle_request(mtp, &msg, usp_data);
    } else {
        imtp_mqtt_handle_response(&response, usp_data);
    }

exit:
    amxc_var_clean(&msg);
    amxc_llist_clean(&response, variant_list_it_free);
    uspl_rx_delete(&usp_data);
}

static void imtp_mqtt_con_read(int fd, void* priv) {
    int retval = -1;
    amxd_object_t* mtp = (amxd_object_t*) priv;
    uspi_con_t* con = (uspi_con_t*) mtp->priv;
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* tlv_protobuf = NULL;
    char* topic = NULL;
    SAH_TRACEZ_INFO(ME, "Incoming message on fd: [%d]", fd);

    retval = uspi_con_read_frame(con, &frame);
    when_failed(retval, exit);

    retval = imtp_mqtt_unpack_frame(frame, &topic, &tlv_protobuf);
    when_failed(retval, exit);

    if(tlv_protobuf != NULL) {
        imtp_mqtt_handle_msg(tlv_protobuf, mtp);
    }

    retval = 0;
exit:
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error reading data from socket, removing accepted connection");
        amxo_connection_remove(mtp_mqtt_get_parser(), fd);
        uspi_con_disconnect(&con);
        mtp->priv = NULL;
    }
    free(topic);
    imtp_frame_delete(&frame);
    return;
}

static int imtp_mqtt_create_socket(amxb_bus_ctx_t* ctx,
                                   const char* client_obj,
                                   amxc_string_t* mqtt_uri) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    const char* method = "CreateListenSocket";

    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "uri", amxc_string_get(mqtt_uri, 0));
    amxc_var_add_key(cstring_t, &args, "receiver_types", "USP");
    retval = amxb_call(ctx, client_obj, method, &args, &ret, 5);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error amxb_invoke function %s, retval = [%d]", method, retval);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return retval;
}

static int imtp_mqtt_set_up(amxb_bus_ctx_t* ctx,
                            uspi_con_t** con,
                            const char* client_obj,
                            const char** msg,
                            void* priv) {
    int retval = -1;
    amxc_string_t mqtt_uri;
    SAH_TRACEZ_INFO(ME, "Requesting new MQTT client listen socket [%s]", client_obj);

    amxc_string_init(&mqtt_uri, 0);
    amxc_string_setf(&mqtt_uri, "usp:/var/run/mqtt/%s", client_obj);

    retval = imtp_mqtt_create_socket(ctx, client_obj, &mqtt_uri);
    when_failed(retval, exit);

    retval = uspi_con_connect(con, amxc_string_get(&mqtt_uri, 0));
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to connect to uri '%s', retval = [%d]\n",
                         amxc_string_get(&mqtt_uri, 0), retval);
        goto exit;
    }

    amxo_connection_add(mtp_mqtt_get_parser(),
                        uspi_con_get_fd(*con),
                        imtp_mqtt_con_read,
                        amxc_string_get(&mqtt_uri, 0),
                        AMXO_BUS,
                        priv);

exit:
    if(retval != 0) {
        if(msg != NULL) {
            *msg = "Failed to set-up MQTT connection";
        }
    }
    amxc_string_clean(&mqtt_uri);
    return retval;
}

int imtp_mqtt_connect(uspi_con_t** con,
                      amxc_var_t* config,
                      const char** msg,
                      void* priv) {
    int retval = -1;
    amxb_bus_ctx_t* bus_ctx = NULL;
    const char* client_obj = GET_CHAR(config, "Reference");
    amxc_var_t* topics = GET_ARG(config, "Topics");

    if((client_obj == NULL) || (client_obj[0] == '\0')) {
        SAH_TRACEZ_ERROR(ME, "Empty MQTT reference");
        if(msg != NULL) {
            *msg = "Empty MQTT reference";
        }
        goto exit;
    }

    bus_ctx = mtp_mqtt_get_bus_ctx();
    if(bus_ctx == NULL) {
        SAH_TRACEZ_ERROR(ME, "Reference [%s] not found", client_obj);
        if(msg != NULL) {
            *msg = "MQTT reference not found";
        }
        goto exit;
    }

    retval = imtp_mqtt_set_up(bus_ctx, con, client_obj, msg, priv);
    when_failed(retval, exit);

    amxc_var_for_each(topic, topics) {
        const char* topic_str = amxc_var_constcast(cstring_t, topic);
        if((topic_str == NULL) || (*topic_str == 0)) {
            continue;
        }
        retval = mqtt_add_subscription(client_obj, topic_str);
        if(retval != 0) {
            *msg = "Failed to add subscriptions";
            goto exit;
        }
    }

    retval = mqtt_start(client_obj);

exit:
    if((retval != 0) && (con != NULL) && (*con != NULL)) {
        if((*con)->imtp_ctx != NULL) {
            amxo_connection_remove(mtp_mqtt_get_parser(), amxb_get_fd((*con)->imtp_ctx));
            uspi_con_disconnect(con);
        }
    }

    return retval;
}

int imtp_mqtt_disconnect(uspi_con_t** con,
                         amxc_var_t* config) {
    int retval = -1;
    const char* client_obj = GET_CHAR(config, "Reference");
    amxb_bus_ctx_t* ctx = NULL;

    when_null(con, exit);
    when_null(*con, exit);

    ctx = (*con)->imtp_ctx;
    mqtt_del_subscription(client_obj, NULL);
    retval = mqtt_stop(client_obj);
    amxo_connection_remove(mtp_mqtt_get_parser(), amxb_get_fd(ctx));
    uspi_con_disconnect(con);

exit:
    return retval;
}