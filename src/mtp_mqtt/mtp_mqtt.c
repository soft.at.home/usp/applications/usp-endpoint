/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <debug/sahtrace.h>
#include <yajl/yajl_gen.h>
#include <amxc/amxc.h>
#include <amxj/amxj_variant.h>
#include <uspi/uspi_connection.h>

#include "mtp/mtp_mqtt.h"

#define ME "mtp_mqtt"

static bool mqtt_subscription_exists(const char* client, const char* topic) {
    bool retval = false;
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();
    amxc_var_t ret;
    amxc_string_t path;

    amxc_var_init(&ret);
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "%sSubscription.[Topic=='%s'].", client, topic);
    amxb_get(bus_ctx, amxc_string_get(&path, 0), 0, &ret, 5);

    if(GETP_ARG(&ret, "0.0") != NULL) {
        retval = true;
    }

    amxc_var_clean(&ret);
    amxc_string_clean(&path);
    return retval;
}

// mqtt_obj will be LocalController.MTP.{i}.MQTT. in case the controller sends a request.
static void mtp_mqtt_add_response_topic(imtp_frame_t* frame,
                                        amxd_object_t* mqtt_obj) {
    amxc_var_t params;
    const char* resp_topic = NULL;
    imtp_tlv_t* tlv_json = NULL;
    amxc_var_t usp_props;
    const char* json_str = NULL;

    amxc_var_init(&usp_props);
    amxc_var_set_type(&usp_props, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&params);

    amxd_object_get_params(mqtt_obj, &params, amxd_dm_access_private);
    resp_topic = GET_CHAR(&params, "ResponseTopicDiscovered");
    if((resp_topic == NULL) || (*resp_topic == 0)) {
        resp_topic = GET_CHAR(&params, "ResponseTopicConfigured");
        when_str_empty(resp_topic, exit);
    }
    amxc_var_add_key(cstring_t, &usp_props, "response-topic", resp_topic);

    amxc_var_cast(&usp_props, AMXC_VAR_ID_JSON);
    json_str = amxc_var_constcast(jstring_t, &usp_props);

    imtp_tlv_new(&tlv_json, imtp_tlv_type_mqtt_props, strlen(json_str),
                 (char*) json_str, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(frame, tlv_json);

exit:
    amxc_var_clean(&usp_props);
    amxc_var_clean(&params);
}

int mqtt_start(const char* client) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();

    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "Enable", true);

    retval = amxb_set(bus_ctx, client, &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "[%s] Enable returned %d", client, retval);
    when_failed(retval, exit);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

int mqtt_stop(const char* client) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();

    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "Enable", false);

    retval = amxb_set(bus_ctx, client, &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "[%s] Disable returned %d", client, retval);
    when_failed(retval, exit);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;

}

int mqtt_add_subscription(const char* client, const char* topic) {
    int retval = -1;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_string_t subscription;
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_string_init(&subscription, 0);

    if(mqtt_subscription_exists(client, topic)) {
        retval = 0;
        goto exit;
    }
    amxc_string_setf(&subscription, "%s.Subscription.", client);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Topic", topic);
    amxc_var_add_key(bool, &args, "Enable", true);

    retval = amxb_add(bus_ctx, amxc_string_get(&subscription, 0), 0, NULL, &args, &ret, 5);
    SAH_TRACEZ_INFO(ME, "Add MQTT subscription for topic %s returned %d", topic, retval);
    when_failed(retval, exit);

exit:
    amxc_string_clean(&subscription);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return retval;
}

int mqtt_del_subscription(const char* client, const char* topic) {
    int retval = -1;
    amxc_var_t ret;
    amxc_string_t subscription;
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();

    amxc_var_init(&ret);
    amxc_string_init(&subscription, 0);

    if(topic == NULL) {
        amxc_string_setf(&subscription, "%sSubscription.*.", client);
        SAH_TRACEZ_INFO(ME, "Deleting all subscription");
    } else {
        amxc_string_setf(&subscription, "%sSubscription.[Topic == '%s'].", client, topic);
        SAH_TRACEZ_INFO(ME, "Deleting subscription %s", amxc_string_get(&subscription, 0));
    }
    retval = amxb_del(bus_ctx, amxc_string_get(&subscription, 0), 0, NULL, &ret, 5);
    when_failed(retval, exit);

exit:
    amxc_string_clean(&subscription);
    amxc_var_clean(&ret);
    return retval;
}

int mqtt_subscribe(const char* client, amxp_slot_fn_t cb, void* priv) {
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();
    const char* filter = "parameters.Status.from == 'Connected' &&"
        "parameters.Status.to in ['Disabled','Error']";

    SAH_TRACEZ_INFO(ME, "Start %s monitor", client);
    return amxb_subscribe(bus_ctx, client, filter, cb, priv);
}

int mqtt_unsubscribe(const char* client, amxp_slot_fn_t cb, UNUSED void* priv) {
    amxb_bus_ctx_t* bus_ctx = mtp_mqtt_get_bus_ctx();

    SAH_TRACEZ_INFO(ME, "Stop %s monitor", client);
    return amxb_unsubscribe(bus_ctx, client, cb, NULL);
}

int mtp_mqtt_imtp_send(uspi_con_t* con,
                       uspl_tx_t* usp_tx,
                       const char* eid,
                       const char* type,
                       const char* notif_topic,
                       amxd_object_t* mqtt_obj) {
    amxc_string_t topic;
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    imtp_tlv_t* tlv_pbuf = NULL;
    int retval = -1;
    size_t topic_len = 0;

    amxc_string_init(&topic, 0);

    if(notif_topic == NULL) {
        amxc_string_setf(&topic, "%s/%s", type, eid);
    } else {
        amxc_string_setf(&topic, "%s", notif_topic);
    }
    topic_len = amxc_string_text_length(&topic);
    SAH_TRACEZ_INFO(ME, "Publish on [%s]", amxc_string_get(&topic, 0));

    imtp_frame_new(&frame);
    imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, topic_len,
                 amxc_string_take_buffer(&topic), 0, IMTP_TLV_TAKE);
    imtp_frame_tlv_add(frame, tlv_topic);
    imtp_tlv_new(&tlv_pbuf, imtp_tlv_type_protobuf_bytes, usp_tx->pbuf_len,
                 usp_tx->pbuf, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(frame, tlv_pbuf);
    mtp_mqtt_add_response_topic(frame, mqtt_obj);

    retval = uspi_con_write_frame(con, frame);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending message to MQTT client");
        goto exit;
    }

    retval = 0;
exit:
    imtp_frame_delete(&frame);
    amxc_string_clean(&topic);
    return retval;
}