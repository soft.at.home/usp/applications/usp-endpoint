/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <uspi/uspi_connection.h>

#include "mtp/mtp_mqtt.h"

#define ME "mtp_mqtt"

void mtp_mqtt_disconnected(UNUSED const char* const sig_name,
                           UNUSED const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* mqtt = (amxd_object_t*) priv;
    amxd_param_t* ref_param = amxd_object_get_param_def(mqtt, "Reference");
    const char* ref = amxc_var_constcast(cstring_t, &ref_param->value);

    // force restart
    SAH_TRACEZ_WARNING(ME, "MQTT unexpected disconnect - force reconnect");
    mqtt_stop(ref);
    mqtt_start(ref);
}

void mtp_mqtt_connection_deleted(UNUSED const char* const sig_name,
                                 const amxc_var_t* const data,
                                 void* const priv) {
    amxc_var_t config;
    amxd_object_t* mqtt = (amxd_object_t*) priv;
    amxd_object_t* mtp_obj = amxd_object_get_parent(mqtt);
    uspi_con_t* con = NULL;

    amxc_var_init(&config);
    when_null(mqtt, exit);

    con = (uspi_con_t*) mqtt->priv;
    when_null(con, exit);

    if(amxc_var_type_of(data) != AMXC_VAR_ID_FD) {
        goto exit;
    }

    if(uspi_con_get_fd(con) != amxc_var_dyncast(fd_t, data)) {
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "mtp MQTT connection closed (%s)",
                    amxd_object_get_name(mqtt, AMXD_OBJECT_NAMED));
    amxp_slot_disconnect_with_priv(NULL, mtp_mqtt_connection_deleted, priv);

    //TODO: trigger signal mtp:connection-closed
    //dm_mtp_set_status(mtp, "Error", "Unexpected disconnect");
    amxd_object_emit_signal(mtp_obj, "mtp:connection-closed", NULL);

exit:
    amxc_var_clean(&config);
    return;
}

void _mtp_mqtt_init(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    amxd_object_t* mqtt = amxd_dm_signal_get_object(mtp_mqtt_get_dm(), data);

    mtp_mqtt_configure_topics(mqtt);
}

void _mtp_mqtt_update_all(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* root = amxd_dm_signal_get_object(mtp_mqtt_get_dm(), data);
    const char* n = amxd_object_get_name(root, AMXD_OBJECT_NAMED);
    amxd_object_t* mtps = amxd_object_findf(root, "MTP.");
    char* endpoint_id = amxd_object_get_value(cstring_t, root, "EndpointID", NULL);
    amxd_trans_t transaction;
    amxc_llist_t mqtt_mtps;
    amxc_string_t topic;

    SAH_TRACEZ_INFO(ME, "mtp MQTT endpoint id has changed");

    amxc_llist_init(&mqtt_mtps);
    amxc_string_init(&topic, 0);
    if(strcmp(n, "LocalAgent") == 0) {
        amxc_string_setf(&topic, "agent/%s", endpoint_id);
    } else {
        amxc_string_setf(&topic, "controller/%s", endpoint_id);
    }
    amxd_trans_init(&transaction);

    amxd_object_resolve_pathf(mtps, &mqtt_mtps, "[Protocol == 'MQTT'].");
    amxc_llist_for_each(it, (&mqtt_mtps)) {
        amxc_string_t* path = amxc_container_of(it, amxc_string_t, it);
        SAH_TRACEZ_INFO(ME, "update %s", amxc_string_get(path, 0));
        amxd_trans_select_pathf(&transaction, "%sMQTT", amxc_string_get(path, 0));
        amxd_trans_set_value(cstring_t, &transaction,
                             "ResponseTopicConfigured", amxc_string_get(&topic, 0));
        amxc_string_delete(&path);
    }

    if(amxd_trans_apply(&transaction, mtp_mqtt_get_dm()) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to update ResponseTopicConfigured for mqtt mtps");
    }

    free(endpoint_id);
    amxc_string_clean(&topic);
    amxd_trans_clean(&transaction);
    amxc_llist_clean(&mqtt_mtps, NULL);
}

void _mtp_mqtt_update_subs(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* mqtt_mtp = amxd_dm_signal_get_object(mtp_mqtt_get_dm(), data);
    amxd_object_t* mtp = amxd_object_get_parent(mqtt_mtp);
    char* status = amxd_object_get_value(cstring_t, mtp, "Status", NULL);
    char* client = amxd_object_get_value(cstring_t, mqtt_mtp, "Reference", NULL);
    const char* from = GETP_CHAR(data, "parameters.ResponseTopicConfigured.from");
    const char* to = GETP_CHAR(data, "parameters.ResponseTopicConfigured.to");

    SAH_TRACEZ_INFO(ME, "Checking mqtt topic");
    if(status && (strcmp(status, "Up") == 0)) {
        SAH_TRACEZ_INFO(ME, "ResponseTopicConfiured has changed - update MQTT subscription");
        SAH_TRACEZ_INFO(ME, "Changed from %s to %s for client %s", from, to, client);
        mqtt_del_subscription(client, from);
        mqtt_add_subscription(client, to);
    }

    free(status);
    free(client);
}
