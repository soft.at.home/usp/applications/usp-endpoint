/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <uspi/uspi_connection.h>
#include <uspi/uspi_utils.h>

#include "usp_main.h"
#include "mtp_utils.h"
#include "mtp/mtp_mqtt.h"

#define ME "mtp_mqtt"

typedef int (* fn_request_new)(uspl_tx_t* usp_tx,
                               const amxc_var_t* request);

// create a list of MQTT subscription topics
static void mtp_mqtt_add_topics(amxc_var_t* config) {
    amxc_var_t* tmp = NULL;
    amxc_var_t* topics = amxc_var_add_new_key(config, "Topics");
    amxc_var_set_type(topics, AMXC_VAR_ID_LIST);

    tmp = GET_ARG(config, "ResponseTopicDiscovered");
    if(amxc_var_constcast(cstring_t, tmp) != NULL) {
        amxc_var_take_it(tmp);
        amxc_var_set_index(topics, 0, tmp, AMXC_VAR_FLAG_DEFAULT);
    }

    tmp = GET_ARG(config, "ResponseTopicConfigured");
    if(amxc_var_constcast(cstring_t, tmp) != NULL) {
        amxc_var_take_it(tmp);
        amxc_var_set_index(topics, 0, tmp, AMXC_VAR_FLAG_DEFAULT);
    }
}

static int mtp_mqtt_init(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    const char* mqtt_path = GET_CHAR(args, "path");
    SAH_TRACEZ_INFO(ME, "Init called for %s", mqtt_path);
    amxd_object_t* mtp = amxd_dm_findf(mtp_mqtt_get_dm(), "%s", mqtt_path);
    amxd_object_t* mqtt = amxd_object_get(mtp, "MQTT");

    return mtp_mqtt_configure_topics(mqtt);
}

static int mtp_mqtt_connect(UNUSED const char* function_name,
                            amxc_var_t* args,
                            amxc_var_t* ret) {
    const char* mtp_path = GET_CHAR(args, "MTP");
    amxd_object_t* mtp_object = amxd_dm_findf(mtp_mqtt_get_dm(), "%s", mtp_path);
    char* alias = amxd_object_get_value(cstring_t, mtp_object, "Alias", NULL);
    amxd_object_t* mqtt = amxd_object_get(mtp_object, "MQTT");
    int retval = -1;
    uspi_con_t* con = NULL;
    amxc_var_t config;
    const char* msg = "";

    amxc_var_init(&config);
    when_not_null(mqtt->priv, exit);
    amxd_object_get_params(mqtt, &config, amxd_dm_access_private);
    mtp_mqtt_add_topics(&config);

    SAH_TRACEZ_INFO(ME, "mtp MQTT connect");
    retval = imtp_mqtt_connect(&con, &config, &msg, mqtt);
    mqtt->priv = con;
    if(retval == 0) {
        const char* ref = GET_CHAR(&config, "Reference");
        mqtt_subscribe(ref, mtp_mqtt_disconnected, mqtt);
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, ret, "retval", retval);
    if(retval != 0) {
        amxc_var_add_key(cstring_t, ret, "message", msg);
    } else {
        amxp_slot_connect(NULL, "connection-deleted", NULL,
                          mtp_mqtt_connection_deleted, mqtt);
    }

exit:
    amxc_var_clean(&config);
    free(alias);
    return 0;
}

static int mtp_mqtt_disconnect(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_INFO(ME, "mtp MQTT disconnect");
    const char* mtp_path = GET_CHAR(args, "MTP");
    amxd_object_t* mtp_object = amxd_dm_findf(mtp_mqtt_get_dm(), "%s", mtp_path);
    amxd_object_t* mqtt = amxd_object_get(mtp_object, "MQTT");
    const char* ref = NULL;
    amxc_var_t config;
    uspi_con_t* con = NULL;

    amxc_var_init(&config);
    when_null(mqtt, exit);
    amxd_object_get_params(mqtt, &config, amxd_dm_access_private);

    ref = GET_CHAR(&config, "Reference");
    mqtt_unsubscribe(ref, mtp_mqtt_disconnected, mqtt);

    con = (uspi_con_t*) mqtt->priv;
    mqtt->priv = NULL;

    amxp_slot_disconnect_with_priv(NULL, mtp_mqtt_connection_deleted, mqtt);
    imtp_mqtt_disconnect(&con, &config);

exit:
    amxc_var_clean(&config);
    amxc_var_clean(ret);
    return 0;
}

static int mtp_mqtt_send_response(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    int retval = -1;
    amxd_dm_t* dm = mtp_mqtt_get_dm();
    const char* mtp_path = GET_CHAR(args, "MTP");
    amxd_object_t* mqtt = amxd_dm_findf(dm, "%s", mtp_path);
    uspi_con_t* con = (uspi_con_t*) mqtt->priv;
    const char* to_id = GET_CHAR(args, "to_id");
    const char* msg_id = GET_CHAR(args, "msg_id");
    uint32_t type = amxc_var_dyncast(uint32_t, GET_ARG(args, "type"));
    uspl_tx_t* usp_tx = NULL;

    SAH_TRACEZ_INFO(ME, "Send response for [%s] using mtp [%s] to [%s]", msg_id, mtp_path, to_id);

    retval = mtp_utils_build_response(type, args, &usp_tx);
    when_failed(retval, exit);
    when_null(usp_tx, exit);

    if(uspi_utils_get_con_type(type) == USPI_CON_CONTROLLER) {
        retval = mtp_mqtt_imtp_send(con, usp_tx, to_id, "agent", NULL, mqtt);
    } else {
        retval = mtp_mqtt_imtp_send(con, usp_tx, to_id, "controller", NULL, mqtt);
    }

exit:
    uspl_tx_delete(&usp_tx);
    return retval;
}

static int mtp_mqtt_usp_operation(amxc_var_t* args, fn_request_new fn) {
    int retval = -1;
    amxd_dm_t* dm = mtp_mqtt_get_dm();
    const char* mtp_path = GET_CHAR(args, "MTP");
    amxd_object_t* mtp_object = amxd_dm_findf(dm, "%s", mtp_path);
    amxd_object_t* controller_object = amxd_object_findf(mtp_object, "^.^");
    amxd_object_t* mqtt = amxd_object_get(mtp_object, "MQTT");
    uspi_con_t* con = (uspi_con_t*) mqtt->priv;
    uspl_tx_t* usp_tx = NULL;
    const char* agent_eid = GET_CHAR(args, "agent");
    char* controller_eid = NULL;
    amxc_var_t* request = GET_ARG(args, "request");

    if((con == NULL) || (con->imtp_ctx == NULL)) {
        SAH_TRACEZ_ERROR(ME, "No connection available");
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "MTP MQTT using [%s]", mtp_path);
    controller_eid = amxd_object_get_value(cstring_t, controller_object, "EndpointID", NULL);
    SAH_TRACEZ_INFO(ME, "Controller endpoint ID = [%s]", controller_eid);
    SAH_TRACEZ_INFO(ME, "Agent endpoint ID = [%s]", agent_eid);

    uspl_tx_new(&usp_tx, controller_eid, agent_eid);
    retval = fn(usp_tx, request);
    when_failed(retval, exit);

    retval = mtp_mqtt_imtp_send(con, usp_tx, agent_eid, "agent", NULL, mqtt);

exit:
    free(controller_eid);
    uspl_tx_delete(&usp_tx);
    return retval;
}

static int mtp_mqtt_usp_get(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_get_new);
}

static int mtp_mqtt_usp_set(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_set_new);
}

static int mtp_mqtt_usp_add(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_add_new);
}

static int mtp_mqtt_usp_del(UNUSED const char* function_name,
                            amxc_var_t* args,
                            UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_delete_new);
}

static int mtp_mqtt_usp_get_supported_dm(UNUSED const char* function_name,
                                         amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_get_supported_dm_new);
}

int mtp_mqtt_configure_topics(amxd_object_t* mqtt) {
    amxd_object_t* root = amxd_object_findf(mqtt, "^.^.^");
    char* endpoint_id = amxd_object_get_value(cstring_t, root, "EndpointID", NULL);
    amxc_string_t topic;

    amxc_string_init(&topic, 0);
    when_null(mqtt, exit);

    if(mqtt->priv == NULL) {
        const char* n = amxd_object_get_name(root, AMXD_OBJECT_NAMED);
        SAH_TRACEZ_INFO(ME, "mtp MQTT initialize");

        if(strcmp(n, "LocalAgent") == 0) {
            amxc_string_setf(&topic, "agent/%s", endpoint_id);
        } else {
            amxc_string_setf(&topic, "controller/%s", endpoint_id);
        }
        amxd_object_set_value(cstring_t, mqtt,
                              "ResponseTopicConfigured", amxc_string_get(&topic, 0));
        SAH_TRACEZ_NOTICE(ME, "Response Topic Configures = %s", amxc_string_get(&topic, 0));
    }

exit:
    free(endpoint_id);
    amxc_string_clean(&topic);
    return 0;
}

static int mtp_mqtt_usp_operate(UNUSED const char* function_name,
                                amxc_var_t* args,
                                UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_operate_new);
}

static int mtp_mqtt_usp_get_instances(UNUSED const char* function_name,
                                      amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    return mtp_mqtt_usp_operation(args, uspl_get_instances_new);
}

static CONSTRUCTOR int mtp_mqtt_module_start(void) {
    amxm_module_t* mod_mqtt = NULL;

    amxm_module_register(&mod_mqtt, NULL, "MQTT");
    amxm_module_add_function(mod_mqtt, "init", mtp_mqtt_init);
    amxm_module_add_function(mod_mqtt, "connect", mtp_mqtt_connect);
    amxm_module_add_function(mod_mqtt, "disconnect", mtp_mqtt_disconnect);
    amxm_module_add_function(mod_mqtt, "send_response", mtp_mqtt_send_response);
    amxm_module_add_function(mod_mqtt, "usp_get", mtp_mqtt_usp_get);
    amxm_module_add_function(mod_mqtt, "usp_set", mtp_mqtt_usp_set);
    amxm_module_add_function(mod_mqtt, "usp_add", mtp_mqtt_usp_add);
    amxm_module_add_function(mod_mqtt, "usp_del", mtp_mqtt_usp_del);
    amxm_module_add_function(mod_mqtt, "usp_get_supported_dm", mtp_mqtt_usp_get_supported_dm);
    amxm_module_add_function(mod_mqtt, "usp_operate", mtp_mqtt_usp_operate);
    amxm_module_add_function(mod_mqtt, "usp_get_instances", mtp_mqtt_usp_get_instances);

    return 0;
}

static DESTRUCTOR int mtp_mqtt_module_stop(void) {
    amxm_module_t* mod_mqtt = amxm_get_module(NULL, "MQTT");
    amxm_module_deregister(&mod_mqtt);
    return 0;
}
