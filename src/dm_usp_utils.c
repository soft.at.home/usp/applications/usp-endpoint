/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include <debug/sahtrace.h>

#include "uspc_version.h"
#include "dm_usp.h"
#include "dm_usp_mtp.h"

#define ME "usp"

static void usp_set_version(amxd_trans_t* transaction) {
    amxc_string_t strversion;
    amxc_string_init(&strversion, 0);

    amxc_string_setf(&strversion, "%d.%d.%d",
                     USPC_VERSION_MAJOR, USPC_VERSION_MINOR, USPC_VERSION_BUILD);
    SAH_TRACEZ_INFO(ME, "USPC version %s", amxc_string_get(&strversion, 0));

    amxd_trans_set_value(cstring_t, transaction,
                         "SoftwareVersion", amxc_string_get(&strversion, 0));
    amxc_string_clean(&strversion);
}

static void usp_set_endpoint_id(amxd_trans_t* transaction) {
    amxc_string_t endpointid;
    const char* type = usp_get_type();
    char hostname[1024] = "";

    amxc_string_init(&endpointid, 0);
    gethostname(hostname, 1023);

    amxc_string_setf(&endpointid, "proto::%s-%s", type, hostname);
    SAH_TRACEZ_INFO(ME, "Endpoint ID %s", amxc_string_get(&endpointid, 0));

    amxd_trans_set_value(cstring_t, transaction,
                         "EndpointID", amxc_string_get(&endpointid, 0));

    amxc_string_clean(&endpointid);
}

int dm_usp_init(const char* type) {
    int retval = -1;
    amxd_object_t* usp_obj = amxd_dm_findf(usp_get_dm(), "Local%s.", type);
    char* eid = NULL;
    amxd_trans_t transaction;

    amxd_trans_init(&transaction);
    when_null(usp_obj, exit);

    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, usp_obj);
    usp_set_version(&transaction);
    eid = amxd_object_get_value(cstring_t, usp_obj, "EndpointID", NULL);
    if((eid == NULL) || (*eid == 0)) {
        SAH_TRACEZ_INFO(ME, "EndpointID is not set in data model");
        usp_set_endpoint_id(&transaction);
    }

    retval = amxd_trans_apply(&transaction, usp_get_dm());

exit:
    free(eid);
    amxd_trans_clean(&transaction);
    return retval;
}
