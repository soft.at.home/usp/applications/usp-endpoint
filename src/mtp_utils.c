/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "usp.h"
#include "usp_main.h"
#include "mtp_utils.h"
#include "mtp/mtp_uds.h"

typedef int (* fn_reply_new)(uspl_tx_t* usp_tx,
                             amxc_llist_t* resp_list,
                             const char* msg_id);

static fn_reply_new reply_builders[] = {
    NULL,                           // ERROR
    NULL,                           // GET
    uspl_get_resp_new,              // GET_RESP
    NULL,                           // NOTIFY
    NULL,                           // SET
    uspl_set_resp_new,              // SET_RESP
    NULL,                           // OPERATE
    uspl_operate_resp_new,          // OPERATE_RESP
    NULL,                           // ADD
    uspl_add_resp_new,              // ADD_RESP
    NULL,                           // DELETE
    uspl_delete_resp_new,           // DELETE_RESP
    NULL,                           // GET_SUPPORTED_DM
    uspl_get_supported_dm_resp_new, // GET_SUPPORTED_DM_RESP
    NULL,                           // GET_INSTANCES
    uspl_get_instances_resp_new,    // GET_INSTANCES_RESP
    uspl_notify_resp_new,           // NOTIFY_RESP
};

void mtp_utils_handle_request(amxd_object_t* mtp, amxc_var_t* msg, uspl_rx_t* usp_data) {
    amxc_string_t signame;
    char* mtp_path = amxd_object_get_path(mtp, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);

    amxc_string_init(&signame, 0);

    amxc_var_add_key(cstring_t, msg, "MTP", mtp_path);
    amxc_var_add_key(cstring_t, msg, "msg_id", uspl_msghandler_msg_id(usp_data));
    amxc_var_add_key(cstring_t, msg, "from_id", uspl_msghandler_from_id(usp_data));
    amxc_var_add_key(cstring_t, msg, "to_id", uspl_msghandler_to_id(usp_data));
    amxc_var_add_key(uint32_t, msg, "type", uspl_msghandler_msg_type(usp_data));

    amxc_string_setf(&signame, "usp:request-%d", uspl_msghandler_msg_type(usp_data));
    amxp_sigmngr_trigger_signal(NULL, amxc_string_get(&signame, 0), msg);

    amxc_string_clean(&signame);
    free(mtp_path);
}

int mtp_utils_build_response(uint32_t type,
                             amxc_var_t* response,
                             uspl_tx_t** usp_tx) {
    int retval = -1;
    const char* msg_id = GET_CHAR(response, "msg_id");
    const char* from_id = GET_CHAR(response, "from_id");
    const char* to_id = GET_CHAR(response, "to_id");
    amxc_var_t* data = GET_ARG(response, "data");
    amxc_llist_t resp_list;
    fn_reply_new builder = NULL;

    amxc_llist_init(&resp_list);

    when_null(usp_tx, exit);
    when_null(msg_id, exit);
    when_null(from_id, exit);
    when_null(to_id, exit);
    when_null(data, exit);

    if((type) >= (sizeof(reply_builders) / sizeof(reply_builders[0]))) {
        goto exit;
    }
    builder = reply_builders[type];

    when_null(builder, exit);
    when_true(amxc_var_type_of(data) != AMXC_VAR_ID_LIST, exit);

    amxc_var_for_each(var, data) {
        amxc_llist_append(&resp_list, &var->lit);
    }

    retval = uspl_tx_new(usp_tx, from_id, to_id);
    when_failed(retval, exit);
    retval = builder(*usp_tx, &resp_list, msg_id);
    when_failed(retval, exit);

exit:
    amxc_llist_clean(&resp_list, variant_list_it_free);
    return retval;
}

// TODO : if provided path is LocalAgent, always return host ctx
bool mtp_utils_is_local(const char* path) {
    amxb_bus_ctx_t* bus_ctx = usp_get_bus_ctx();
    amxd_path_t obj_path;
    int retval = -1;
    bool is_local = true;
    amxc_var_t exists;

    amxc_var_init(&exists);
    amxd_path_init(&obj_path, path);

    when_str_empty(path, exit);
    amxd_path_setf(&obj_path, false, "%s", path);

    retval = amxb_describe(bus_ctx, amxd_path_get(&obj_path, AMXD_OBJECT_TERMINATE), AMXB_FLAG_EXISTS, &exists, 5);
    if((retval != 0) || !amxc_var_dyncast(bool, GETI_ARG(&exists, 0))) {
        is_local = false;
    }

exit:
    amxc_var_clean(&exists);
    amxd_path_clean(&obj_path);
    return is_local;
}

amxb_bus_ctx_t* mtp_utils_get_ctx(const char* path) {
    amxb_bus_ctx_t* bus_ctx = usp_get_bus_ctx();

    if(!mtp_utils_is_local(path)) {
        bus_ctx = mtp_uds_get_host_ctx(USPI_CON_CONTROLLER);
    }

    return bus_ctx;
}
