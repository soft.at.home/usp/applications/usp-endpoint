/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include "dm_usp_mtp.h"

#define ME "uspe"

static int usp_mtp_connect(amxd_object_t* mtp,
                           char** msg) {
    int retval = -1;
    char* path = amxd_object_get_path(mtp, AMXD_OBJECT_TERMINATE);
    amxd_param_t* p = amxd_object_get_param_def(mtp, "Protocol");
    const char* protocol_name = amxc_var_constcast(cstring_t, &p->value);
    amxc_var_t args;
    amxc_var_t ret;
    SAH_TRACEZ_INFO(ME, "%s connect", path);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "MTP", path);
    amxc_var_init(&ret);

    if(amxm_execute_function(NULL, protocol_name, "connect", &args, &ret) == 0) {
        *msg = amxc_var_take(cstring_t, GET_ARG(&ret, "message"));
        retval = amxc_var_dyncast(int32_t, GET_ARG(&ret, "retval"));
    } else {
        *msg = strdup("Connect method failed or is not available");
        SAH_TRACEZ_ERROR(ME, "%s", *msg);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(path);
    return retval;
}

static int usp_mtp_disconnect(amxd_object_t* mtp) {
    int retval = -1;
    char* path = amxd_object_get_path(mtp, AMXD_OBJECT_TERMINATE);
    amxd_param_t* p = amxd_object_get_param_def(mtp, "Protocol");
    const char* protocol_name = amxc_var_constcast(cstring_t, &p->value);
    amxc_var_t args;
    amxc_var_t ret;
    SAH_TRACEZ_INFO(ME, "%s disconnect", path);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "MTP", path);
    amxc_var_init(&ret);

    if(amxm_execute_function(NULL, protocol_name, "disconnect", &args, &ret) == 0) {
        retval = amxc_var_dyncast(int32_t, GET_ARG(&ret, "retval"));
    } else {
        SAH_TRACEZ_ERROR(ME, "No disconnect method available");
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(path);
    return retval;
}

// called from event handlers
// * _usp_mtp_added - when a new MTP instance has been added
//                   with Enable = true and LC.Enable = true
// * _usp_mtp_toggle - when Enable has been toggled from false to true
//                    and LC.Enable = true
// * _usp_toggle - When LC.Enable toggled from false to true
//                and MTP.Enable = true
int usp_mtp_enable(UNUSED amxd_object_t* mtps,
                   amxd_object_t* mtp,
                   UNUSED void* priv) {
    char* msg = NULL;
    char* path = amxd_object_get_path(mtp, AMXD_OBJECT_TERMINATE);

    when_true(dm_mtp_is_up(mtp), exit);

    SAH_TRACEZ_INFO(ME, "%s enabled", path);

    // set-up the connection and updates the MTP status accordingly
    if(usp_mtp_connect(mtp, &msg) == 0) {
        dm_mtp_set_status(mtp, "Up", NULL);
    } else {
        dm_mtp_set_status(mtp, "Error", msg);
    }

exit:
    free(path);
    free(msg);
    return 0;
}

// called from event handlers
// * _usp_mtp_toggle - when Enable has been toggled from true to false
// * _lc_toggle - When LC.Enable is toggled from true to false
//                and MTP.Status = Up
int usp_mtp_disable(UNUSED amxd_object_t* mtps,
                    UNUSED amxd_object_t* mtp,
                    UNUSED void* priv) {
    when_true(!dm_mtp_is_up(mtp), exit);
    SAH_TRACEZ_INFO(ME, "Local%s.MTP.{i}. disabled", usp_get_type());

    usp_mtp_disconnect(mtp);
    dm_mtp_set_status(mtp, "Down", NULL);

exit:
    return 0;
}

// Event callback function - set in odl file
//      on event "dm:instance-added" call usp_mtp_added
//                filter 'path == "LocalController.MTP."';
void _usp_mtp_added(UNUSED const char* const sig_name,
                    const amxc_var_t* const data,
                    UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Local%s.MTP added", usp_get_type());
    amxd_dm_t* dm = usp_get_dm();
    amxd_object_t* mtps = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* lc = amxd_object_get_parent(mtps);
    uint32_t index = GET_UINT32(data, "index");
    amxd_object_t* mtp = amxd_object_get_instance(mtps, NULL, index);

    bool controller_enabled = amxd_object_get_value(bool, lc, "Enable", NULL);
    bool mtp_enabled = amxd_object_get_value(bool, mtp, "Enable", NULL);
    char* protocol = amxd_object_get_value(cstring_t, mtp, "Protocol", NULL);

    amxd_object_add_mib(mtp, protocol);
    if(controller_enabled && mtp_enabled) {
        usp_mtp_enable(mtps, mtp, NULL);
    }

    free(protocol);
    return;
}

// Event callback function - set in odl file
//    on event "dm:object-changed" call usp_mtp_toggle
//             filter 'path matches "^LocalController\.MTP\.[0-9]*\.$" &&
//             contains("parameters.Enable")';
void _usp_mtp_toggle(UNUSED const char* const sig_name,
                     const amxc_var_t* const data,
                     UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "Local%s.MTP.{i}.Enable toggeled", usp_get_type());
    amxd_dm_t* dm = usp_get_dm();
    amxd_object_t* mtp = amxd_dm_signal_get_object(dm, data);
    amxd_object_t* mtps = amxd_object_get_parent(mtp);
    amxd_object_t* lc = amxd_object_get_parent(mtps);

    bool controller_enabled = amxd_object_get_value(bool, lc, "Enable", NULL);
    bool mtp_enabled = amxd_object_get_value(bool, mtp, "Enable", NULL);

    if(controller_enabled && mtp_enabled) {
        usp_mtp_enable(mtps, mtp, NULL);
    } else {
        usp_mtp_disable(mtps, mtp, NULL);
    }
}

void _usp_mtp_connection_closed(UNUSED const char* const sig_name,
                                const amxc_var_t* const data,
                                UNUSED void* const priv) {
    amxd_dm_t* dm = usp_get_dm();
    amxd_object_t* mtp = amxd_dm_signal_get_object(dm, data);

    dm_mtp_set_status(mtp, "Error", "Connection closed");
}